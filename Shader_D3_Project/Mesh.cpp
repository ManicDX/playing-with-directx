#include "Mesh.h"



Mesh::Mesh()
{
}


Mesh::~Mesh()
{
}

bool Mesh::CreateMesh(ID3D11Device* g_pd3dDevice, const char* filePath) {

	HRESULT hr = S_OK;

	//load mesh
	obj.loadOBJ(filePath);

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(VertexType) * obj.m_vertexCount;
	//bd.ByteWidth = sizeof(SimpleVertex) * 24;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = obj.vertices;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;
	hr = g_pd3dDevice->CreateBuffer(&bd, &InitData, &pVertexBuffer);
	if (FAILED(hr))
		return hr;

	// Set vertex buffer
	stride = sizeof(VertexType);
	//UINT stride = sizeof(SimpleVertex);
	offset = 0;
	//g_pImmediateContext->IASetVertexBuffers(0, 1, &pVertexBuffer, &stride, &offset);


	//bd.Usage = D3D11_USAGE_DEFAULT;
	//bd.ByteWidth = sizeof( unsigned int ) * obj.m_indexCount;
	////bd.ByteWidth = sizeof(WORD) * 36;
	//bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	//bd.CPUAccessFlags = 0;
	InitData.pSysMem = obj.indices;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;

	// Set up the description of the static index buffer.
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(unsigned int) * obj.m_indexCount;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	bd.StructureByteStride = 0;

	hr = g_pd3dDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer);
	if (FAILED(hr))
		return hr;



	return true;
}

void Mesh::Release() {
	if (pVertexBuffer) pVertexBuffer->Release();
	if (pIndexBuffer) pIndexBuffer->Release();
}