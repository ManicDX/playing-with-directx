
VS_OUTPUT RenderShadowsVS(
     float3 position : POSITION,
     float3 normal : NORMAL,
     float2 vTexCoord0 : TEXCOORD0 )
{
     VS_OUTPUT Output;

     //generate the world-view-projection matrix
     float4x4 wvp = mul(mul(g_mWorld, g_mCameraView), g_mCameraProj);
     
     //transform the input position to the output
     Output.Position = mul(float4(position, 1.0), wvp);

     //transform the normal to world space
     Output.vNormal =  mul(normal, g_mWorld);
     
     //do not transform the position needed for the
     //shadow map determination
     Output.vPos = float4(position,1.0);
     
     //pass the texture coordinate as-is
     Output.TextureUV = vTexCoord0;
    
     //return the output structure
     return Output;
}


PS_OUTPUT RenderShadowsPS( PS_INPUT In ) 
{ 
    PS_OUTPUT Output;
    
    // Standard lighting equation
    float4 vTotalLightDiffuse = float4(0,0,0,1);
    float3 lightDir = normalize(g_LightPos-In.vPos);  // direction of light
    vTotalLightDiffuse += g_LightDiffuse * max(0,dot(In.vNormal, lightDir)); 
    vTotalLightDiffuse.a = 1.0f;
	
	// Now, consult the ShadowMap to see if we're in shadow
	float4 lightingPosition = GetPositionFromLight(In.vPos);// Get our position on the shadow map

	// Get the shadow map depth value for this pixel   
	float2 ShadowTexC = 0.5 * lightingPosition.xy / lightingPosition.w + float2( 0.5, 0.5 );
	ShadowTexC.y = 1.0f - ShadowTexC.y;
	
	float shadowdepth = tex2D(ShadowMapSampler, ShadowTexC).r;    

	// Check our value against the depth value
	float ourdepth = 1 - (lightingPosition.z / lightingPosition.w);
	
	// Check the shadowdepth against the depth of this pixel
	// a fudge factor is added to account for floating-point error
	if (shadowdepth-0.03 > ourdepth)
	{
		// we're in shadow, cut the light
		vTotalLightDiffuse = float4(0,0,0,1);
	};
	
	    Output.RGBColor = tex2D(MeshTextureSampler, In.TextureUV) * 
        (vTotalLightDiffuse + g_LightAmbient);
        
    return Output;
    
}
	
	
	
	