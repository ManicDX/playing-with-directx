

//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
Texture2D txDiffuse : register( t0 );
Texture2D txdepthMap : register( t1 );
Texture2D qMap : register( t2 );
Texture2D txnormalMap : register( t3 );
Texture2D txlinearDepthMap : register( t4 );

SamplerState samLinear : register( s0 );
SamplerState samDP : register( s1 );
SamplerComparisonState ShadowSampler
{
   // sampler state
   Filter = MIN_MAG_MIP_POINT;
   AddressU = Clamp;
   AddressV = Clamp;

   // sampler comparison state
   ComparisonFunc = LESS;
   //ComparisonFilter = COMPARISON_MIN_MAG_MIP_POINT;
};

cbuffer cbNeverChanges : register( b0 )
{
    matrix View;
	float4 LightPos;
	matrix LightMVP;
	matrix LightProj;
	matrix BiasMat;
};

cbuffer cbChangeOnResize : register( b1 )
{
    matrix Projection;
};

cbuffer cbChangesEveryFrame : register( b2 )
{
    matrix World;
    float4 vMeshColor;
	float4 Camera;
};



  static const float total_strength = 1.0;
  static const float base = 0.2;

  static const float area = 0.0075;
  static const float falloff = 0.000001;

  static const float radius = 0.0002;

  static const int samples = 16;
  static float3 sample_sphere[samples] = {
      float3( 0.5381, 0.1856,-0.4319), float3( 0.1379, 0.2486, 0.4430),
      float3( 0.3371, 0.5679,-0.0057), float3(-0.6999,-0.0451,-0.0019),
      float3( 0.0689,-0.1598,-0.8547), float3( 0.0560, 0.0069,-0.1843),
      float3(-0.0146, 0.1402, 0.0762), float3( 0.0100,-0.1924,-0.0344),
      float3(-0.3577,-0.5301,-0.4358), float3(-0.3169, 0.1063, 0.0158),
      float3( 0.0103,-0.5869, 0.0046), float3(-0.0897,-0.4940, 0.3287),
      float3( 0.7119,-0.0154,-0.0918), float3(-0.0533, 0.0596,-0.5411),
      float3( 0.0352,-0.0631, 0.5460), float3(-0.4776, 0.2847,-0.0271)
  };


const float noiseScale = float2(800.0/4.0, 600.0/4.0); // screen = 800x600

//--------------------------------------------------------------------------------------

struct VS_INPUT
{
    float4 Pos : POSITION;
    float2 Tex : TEXCOORD0;
	float3 Normal : NORMAL;
};

struct PS_INPUT
{
    float4 Pos : SV_POSITION;
    float2 Tex : TEXCOORD0;
	float3 Normal : NORMAL;
	float3 WorldPos : TEXCOORD1;
	float3 ViewDirection: TEXCOORD2;
	float4 shadowCoord : TEXCOORD3;
	float3 lightPos : TEXCOORD4;
};

struct PS_OUT
{
	float2 Tex : TEXCOORD0;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT VS( VS_INPUT input )
{
    PS_INPUT output = (PS_INPUT)0;
	//input.Pos.w = 1.0f;
    output.Pos = mul( input.Pos, World );
    output.Pos = mul( output.Pos, View );
    output.Pos = mul( output.Pos, Projection );
	
    output.Tex = input.Tex;
    
    // Calculate the normal vector against the world matrix only.
    output.Normal = mul(input.Normal, (float3x3)World);
	
    // Normalize the normal vector.
    output.Normal = normalize(output.Normal);

	output.WorldPos = mul(input.Pos, World);
	
	//For specular
	 // Determine the viewing direction based on the position of the camera and the position of the vertex in the world.
    output.ViewDirection = Camera.xyz - output.WorldPos.xyz;
	
    // Normalize the viewing direction vector.
    output.ViewDirection = normalize(output.ViewDirection);
	
	
	//ShadowMapping stuff
	//matrix depthMVP = BiasMat * (LightProj * LightMVP);
	output.shadowCoord = mul(normalize(input.Pos), World);
	output.shadowCoord = mul(output.shadowCoord, LightMVP);
	output.shadowCoord = mul(output.shadowCoord, LightProj);
    output.shadowCoord = mul(output.shadowCoord, BiasMat);
	
	
    // Calculate the position of the vertex in the world.
    float4 worldPosition = mul(input.Pos, World);
    
    // Determine the light position based on the position of the light and the position of the vertex in the world.
    output.lightPos = 	LightPos.xyz - worldPosition.xyz;
    
    // Normalize the light position vector.
    output.lightPos = normalize(output.lightPos);
	
	//output.lightPos = LightPos.xyz - input.Pos.xyz;
	
    return output;
	

}


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS( PS_INPUT input) : SV_Target
{
	
	float3 lp =float3(0.0f, 5.0f, -5.0f);
	float3 LightDir = float3(1.0f, 1.0f, 0.0f);
    float4 LightColour = float4(1.0f, 1.0f, 1.0f, 1.0f);
	
	float4 AmbientColor = float4(0.5f,0.5f,0.5f,1.0f);
	float AmbientIntensity = 1.0f;
	
	// BASIC COLOUR
	//return txDiffuse.Sample( samLinear, input.Tex ) * vMeshColor;
	//return txDiffuse.Sample( samLinear, input.Tex);
	//return float4(0.0f, 0.0f, 1.0f, 1.0f);
	
	//  RED TOP DOWN LIGHTING
	//float3 L;
	//L = normalize(LightPos - input.Normal);   
	//float max; 
	//max = dot(input.Normal, L);
    //float4 Idiff;
	//Idiff = LightColour * max;  
    //return Idiff = clamp(Idiff, 0.0, 1.0); 
	
	// DIFFUSE
	//float multiplier = dot(LightDir, input.Normal);
	//multiplier = clamp(multiplier, 0.1f, 1.0f);
	//float4 Idiff;
	//Idiff = txDiffuse.Sample( samLinear, input.Tex );
	////texture2D(s_texture, float2(input.Tex[0], 1.0f - input.Tex[1])).rgb;
	//return Idiff * multiplier;
	

	float3 ViewVector = float3(1.0f, 0.0f, 0.0f);
	float Shininess = 200.0f;
	float4 SpecularColor = float4(1.0f, 1.0f, 1.0f, 1.0f);    
	float SpecularIntensity = 1.0f;
	
	float3 DiffuseLightDirection = float3(0.0f, 1.0f, -1.0f);
	float4 DiffuseColor = float4(1.0f, 1.0f, 1.0f, 1.0f);
	float4 LightDiffuseColor = float4(1.0f, 1.0f, 1.0f, 1.0f);
	float DiffuseIntensity = 0.5f;
	
	
	//Specular
	// Sample the pixel color from the texture using the sampler at this texture coordinate location.

	//float3 Norm = normalize(input.Normal);
	////float3 ld = normalize(LightDir)
	//// Get ambient light
	//AmbientColor *= AmbientIntensity;
	//// Get diffuse light
	//DiffuseColor = (DiffuseIntensity * DiffuseColor) * saturate(dot(LightDir,Norm));
	//float3 Half = normalize(LightDir + normalize(input.ViewDirection));
	//float specular = pow(saturate(dot(Norm,Half)),25);
	//float4 Color = AmbientColor + DiffuseColor + ((SpecularColor * SpecularIntensity) * specular);
	//return Color;
	
	
	////Phong - Blinn  without tex
	//
	//	float3 lightDir = lp - input.WorldPos; //3D position in space of the surface
	//	float distance = length( lightDir );
	//	lightDir = lightDir / distance; // = normalize( lightDir );
	//	distance = distance * distance; //This line may be optimised using Inverse square root
    //    //
	//	////Intensity of the diffuse light. Saturate to keep within the 0-1 range.
	//	float NdotL = dot( input.Normal, lightDir );
	//	float intensity = saturate( NdotL );
    //    //
	//	//// Calculate the diffuse light factoring in light color, power and the attenuation
	//	float4 Diffuse = intensity * DiffuseColor * DiffuseIntensity / distance;
    //    //
	//	////Calculate the half vector between the light vector and the view vector.
	//	////This is typically slower than calculating the actual reflection vector
	//	//// due to the normalize function's reciprocal square root
	//	float3 viewDir = normalize(-input.Pos);
	//	float3 H = normalize( lightDir + viewDir);
    //    //
	//	////Intensity of the specular light
	//	float NdotH = dot( input.Normal, H );
	//	intensity = pow( saturate( NdotH ), SpecularIntensity );
    //    //
	//	////Sum up the specular light factoring
	//	float4 Specular = intensity * SpecularColor * SpecularIntensity / distance; 
	//	//
	//	return saturate(AmbientColor + (DiffuseColor * LightDiffuseColor * Diffuse * 0.6) + (SpecularColor * SpecularColor * Specular * 0.5) );
	

	
	//ShadowMapping stuff
	//float bias;
    //float4 color;
    //float2 projectTexCoord;
    //float depthValue;
    //float lightDepthValue;
    //float lightIntensity;
    //float4 textureColor;
	//// Set the bias value for fixing the floating point precision issues.
    //bias = 0.001f;
    //
    //// Set the default output color to the ambient light value for all pixels.
    //color = AmbientColor;
    //
    //
    //// Calculate the projected texture coordinates.
    //projectTexCoord.x =  input.shadowCoord.x / input.shadowCoord.w / 2.0f + 0.5f;
    //projectTexCoord.y = -input.shadowCoord.y / input.shadowCoord.w/ 2.0f + 0.5f;
    //
    //
    //// Determine if the projected coordinates are in the 0 to 1 range.  If so then this pixel is in the view of the light.
    //if((saturate(projectTexCoord.x) == projectTexCoord.x) && (saturate(projectTexCoord.y) == projectTexCoord.y))
    //{
    //
    //    // Sample the shadow map depth value from the depth texture using the sampler at the projected texture coordinate location.
    //    depthValue = txdepthMap.Sample(samDP, projectTexCoord).r;
	//	//depthValue = txDiffuse.Sample(samDP, projectTexCoord).r;
    //
    //
    //    // Calculate the depth of the light.
    //    lightDepthValue = input.shadowCoord.z / input.shadowCoord.w;
    //    
    //    // Subtract the bias from the lightDepthValue.
    //    lightDepthValue = lightDepthValue - bias;
    //    
    //    
    //    // Compare the depth of the shadow map value and the depth of the light to determine whether to shadow or to light this pixel.
    //    // If the light is in front of the object then light the pixel, if not then shadow this pixel since an object (occluder) is casting a shadow on it.
    //    if(lightDepthValue < depthValue)
    //    {
    //    
    //    
    //        // Calculate the amount of light on this pixel.
    //        lightIntensity = saturate(dot(input.Normal, input.lightPos));
    //    
    //        if(lightIntensity > 0.0f)
    //        {
    //            // Determine the final diffuse color based on the diffuse color and the amount of light intensity.
    //            color += (DiffuseColor * lightIntensity);
    //    
    //            // Saturate the final light color.
    //            color = saturate(color);
    //        }
    //    }
    //}
    //
    //// Sample the pixel color from the texture using the sampler at this texture coordinate location.
    //textureColor = txDiffuse.Sample( samLinear, input.Tex);
    //
    //// Combine the light and texture color.
    //color = color * textureColor;
    //
    //return color;
	//
	//	//temp
	//return float4(input.shadowCoord.z, input.shadowCoord.z, input.shadowCoord.z, 1.0f);
	
	
	
	////Ye olde shadowmap
	float3 perspectiveDivide = float3(input.shadowCoord.xyz / input.shadowCoord.w );
	perspectiveDivide.x = perspectiveDivide.x / 2 + 0.5;
	perspectiveDivide.y = perspectiveDivide.y / 2 + 0.5;
	float lightDepthPD = perspectiveDivide.z ;
	
	
	float camDepth = txdepthMap.Sample(samLinear, perspectiveDivide.xy).r;
	
	float bias = 0.005f;
	
	float visibility = 0.2f;
	if(camDepth < lightDepthPD - bias)
	{
		visibility = 1.0f;
	}
	
	float3 N = normalize(input.Normal);
	float3 L = normalize(input.lightPos); //Light Direction
	
	float Lambert = max(0.0f, dot(N,L));
		
	float3 diffuse = visibility * (Lambert * LightColour * DiffuseColor);
	float4 newDiff = float4(diffuse.xyz, 1.0f);
	
	float3 o_color = diffuse.xyz * txDiffuse.Sample( samLinear, input.Tex).rgb;
	return float4(o_color.xyz,1.0f);
	
	//return float4(lightDepthPD, lightDepthPD, lightDepthPD,1.0f);
	//return float4(camDepth, camDepth, camDepth,1.0f);
}

//*****************DEPTH BUFFER stuff***********************//

PS_INPUT VS_Depth( VS_INPUT input )
{
	PS_INPUT output = (PS_INPUT)0;
	
	//input.Pos.w = 1.0f;
	//output.Pos = normalize(output.Pos);
    output.Pos = mul( input.Pos, World );
    output.Pos = mul( output.Pos, LightMVP );
    output.Pos = mul( output.Pos, LightProj );
	//output.Pos = mul( output.Pos, BiasMat );
	
	//output.Pos = normalize(output.Pos);
	
	return output;
}

float4 PS_Depth(PS_INPUT input) : SV_Target
{
	float num = input.Pos.z/input.Pos.w;
	//float num = input.Pos.z;
	//return input.Pos.z;
	return float4(num, num, num, 1.0f);
}

//*****************Phong-Blinn**********************//


//Phonn-Blinn shader
PS_INPUT VS_Phong( VS_INPUT input )
{
    PS_INPUT output = (PS_INPUT)0;
	input.Pos.w = 1.0f;
    output.Pos = mul( input.Pos, World );
    output.Pos = mul( output.Pos, LightMVP );
    output.Pos = mul( output.Pos, LightProj );
	
    output.Tex = input.Tex;
    
    // Calculate the normal vector against the world matrix only.
    output.Normal = mul(input.Normal, (float3x3)World);
	
    // Normalize the normal vector.
    output.Normal = normalize(output.Normal);

	output.WorldPos = mul(input.Pos, World);
	
	//For specular
	 // Determine the viewing direction based on the position of the camera and the position of the vertex in the world.
    output.ViewDirection = Camera.xyz - output.WorldPos.xyz;
	
    // Normalize the viewing direction vector.
    output.ViewDirection = normalize(output.ViewDirection);
	
	return output;
}

float4 PS_Phong(PS_INPUT input) : SV_Target
{
	
	float3 lp =float3(-5.0f, 0.1f, 0.0f);
	float3 LightDir = float3(0.0f, -1.0f, 0.0f);
    float4 LightColour = float4(1.0f, 1.0f, 1.0f, 1.0f);
	
	float4 AmbientColor = float4(0.3f,0.0f,0.0f,1.0f);
	float AmbientIntensity = 1.0f;
	
	float3 ViewVector = float3(1.0f, 0.0f, 0.0f);
	float Shininess = 500.0f;
	float4 SpecularColor = float4(1.0f, 1.0f, 1.0f, 1.0f);    
	float SpecularIntensity = 5.0f;
	
	float3 DiffuseLightDirection = float3(0.0f, -1.0f, 0.0f);
	float4 DiffuseColor = float4(0.5f, 0.5f, 0.5f, 1.0f);
	float4 LightDiffuseColor = float4(1.0f, 1.0f, 1.0f, 1.0f);
	float DiffuseIntensity = 5.0f;
	
		////Phong - Blinn  without tex
	
	float3 lightDir = lp - input.WorldPos; //3D position in space of the surface
	float distance = length( lightDir );
	lightDir = lightDir / distance; // = normalize( lightDir );
	distance = distance * distance; //This line may be optimised using Inverse square root
    //
	////Intensity of the diffuse light. Saturate to keep within the 0-1 range.
	float NdotL = dot( input.Normal, lightDir );
	float intensity = saturate( NdotL );
    //
	//// Calculate the diffuse light factoring in light color, power and the attenuation
	float4 Diffuse = intensity * DiffuseColor * DiffuseIntensity / distance;
    //
	////Calculate the half vector between the light vector and the view vector.
	////This is typically slower than calculating the actual reflection vector
	//// due to the normalize function's reciprocal square root
	float3 viewDir = normalize(-input.Pos);
	float3 H = normalize( lightDir + viewDir);
    //
	////Intensity of the specular light
	float NdotH = dot( input.Normal, H );
	intensity = pow( saturate( NdotH ), SpecularIntensity );
    //
	////Sum up the specular light factoring
	float4 Specular = intensity * SpecularColor * SpecularIntensity / distance; 
	//
	
	return saturate(AmbientColor + (DiffuseColor * LightDiffuseColor * Diffuse * 0.6) + (SpecularColor * SpecularColor * Specular * 0.5));
}


//Toon Stuff stuff
PS_INPUT VS_CelShading( VS_INPUT input )
{
    PS_INPUT output = (PS_INPUT)0;
	input.Pos.w = 1.0f;
    output.Pos = mul( input.Pos, World );
    output.Pos = mul( output.Pos, View );
    output.Pos = mul( output.Pos, Projection );
	
    output.Tex = input.Tex;
    
    // Calculate the normal vector against the world matrix only.
    output.Normal = mul(input.Normal, (float3x3)World);
	
    // Normalize the normal vector.
    output.Normal = normalize(output.Normal);

	output.WorldPos = mul(input.Pos, World);
	
	//For specular
	 // Determine the viewing direction based on the position of the camera and the position of the vertex in the world.
    output.ViewDirection = Camera.xyz - output.WorldPos.xyz;
	
    // Normalize the viewing direction vector.
    output.ViewDirection = normalize(output.ViewDirection);
	
	// Calculate the position of the vertex in the world.
    float4 worldPosition = mul(input.Pos, World);
    
    // Determine the light position based on the position of the light and the position of the vertex in the world.
    output.lightPos = 	LightPos.xyz - worldPosition.xyz;
    
    // Normalize the light position vector.
    output.lightPos = normalize(output.lightPos);
	
	return output;
}

float4 PS_CelShading(PS_INPUT input) : SV_Target
{
	// The color of the diffuse light
	float4 DiffuseColor = float4(1, 1, 1, 1);
	
	// The intensity of the diffuse light
	float DiffuseIntensity = 1.0;

	//float cel;
	//float3 gooch;
	//
	//
	////cel
	// float3 N = normalize(input.Normal); 
    //
	//
	//float3 L = normalize(input.lightPos - input.Pos);
	//
	//
	//float Lambert = max(0.0, dot(N, L)); 
	////if your doing additive lighting you add diffuse with specular if you dont max it you will end up with a negative number 
	//
	//
	////MAKE A 2D TOON SHADE TEXTURE....
	//cel = qMap.Sample(samDP, float2(Lambert, 0.0f)).r; 
    //
    //
    //
	////Gooch
    //
	//float3 cool = float3(0.68f, 0.92f, 0.92f); 
	//float3 warm = float3(0.99f, 0.1f, 0.0f); 
	//
	//
	//
	//float Gooch = dot(N, L) * 0.5f + 0.5f; 
	//
	//
	////dot product returns a range from -1 to 1 
	////thats why we use the cut off
	////[-1, 1] * 0.5 + 0.5 = [0,1] = SERIALIZATION
	//
	//float3 GoochSample = lerp(cool, warm, Gooch); 
	////mix is the function for lerp
	//
	//float3 diffuse = Lambert * txDiffuse.Sample(samLinear, input.Tex).rgb; 
	//
	//gooch = diffuse + GoochSample; 
	//
    //
	//float4 diff = float4(1.0f, 0.0f, 0.0f, 1.0f);//txDiffuse.Sample(samLinear, input.Tex);
    //
	//return cel * diff; 
	//return float4(gooch, 1.0f);

	//normalColour.rgb = data.normalObj * 0.5 + 0.5; 
	//fragColour.rgb = gooch;
 
	//float num = input.Pos.z;//input.Pos.w;
	//return input.Pos.z;
	//return qMap.Sample(samDP, input.Tex);
	//return float4(num, num, num, 1.0f);
	
	    // Calculate diffuse light amount
    float intensity = dot(normalize(input.lightPos), input.Normal);
    if(intensity < 0)
        intensity = 0;
 
    // Calculate what would normally be the final color, including texturing and diffuse lighting
    float4 color = txDiffuse.Sample(samLinear, input.Tex) * DiffuseColor * DiffuseIntensity;
    color.a = 1;
 
    // Discretize the intensity, based on a few cutoff points
    if (intensity > 0.95)
        color = float4(1.0,1,1,1.0) * color;
    else if (intensity > 0.5)
        color = float4(0.7,0.7,0.7,1.0) * color;
    else if (intensity > 0.05)
        color = float4(0.35,0.35,0.35,1.0) * color;
    else
        color = float4(0.1,0.1,0.1,1.0) * color;
 
    return color;
}

//Outline shader
// The vertex shader that does the outlines
PS_INPUT VS_OutlineShader(VS_INPUT input)
{
	float LineThickness = 0.3f;
	
    PS_INPUT output = (PS_INPUT)0;
    
    // Calculate where the vertex ought to be.  This line is equivalent
    // to the transformations in the CelVertexShader.
    float4 original = mul( input.Pos, World );
    original = mul( original, View );
    original = mul( original, Projection );
    
    // Calculates the normal of the vertex like it ought to be.
	float4 normal = mul( input.Normal, World );
    normal = mul( normal, View );
    normal = mul( normal, Projection );
    
    // Take the correct "original" location and translate the vertex a little
    // bit in the direction of the normal to draw a slightly expanded object.
    // Later, we will draw over most of this with the right color, except the expanded
    // part, which will leave the outline that we want.
    output.Pos    = original + (LineThickness * normal);
 
    return output;
}
 
// The pixel shader for the outline.  It is pretty simple:  draw everything with the
// correct line color.
float4 PS_OutlineShader(PS_INPUT input) : SV_Target
{
    return float4(0, 0, 0, 1);
}



PS_INPUT VS_PassThru(VS_INPUT input)
{
    PS_INPUT output = (PS_INPUT)0;
	input.Pos.w = 1.0f;
    output.Pos = mul( input.Pos, World );
    output.Pos = mul( output.Pos, View );
    output.Pos = mul( output.Pos, Projection );
	
    output.Tex = input.Tex;
    
    // Calculate the normal vector against the world matrix only.
    output.Normal = mul(input.Normal, (float3x3)World);
	
    // Normalize the normal vector.
    output.Normal = normalize(output.Normal);

	output.WorldPos = mul(input.Pos, World);
	
	//For specular
	 // Determine the viewing direction based on the position of the camera and the position of the vertex in the world.
    output.ViewDirection = Camera.xyz - output.WorldPos.xyz;
	
    // Normalize the viewing direction vector.
    output.ViewDirection = normalize(output.ViewDirection);
	
	return output;
}
 
//************Normal Mapping **********************//
float4 PS_NormalBuffer(PS_INPUT input) : SV_Target
{
    return float4(input.Normal, 1.0f);
}


//************Diffuse G-Buffer  **********************//
float4 PS_DiffuseGBuffer(PS_INPUT input) : SV_Target
{
    return float4(input.Normal, 1.0f);
	//return input.color;
}



//**************SSAO***********************//
PS_INPUT VS_SSAO(VS_INPUT input)
{
    PS_INPUT output = (PS_INPUT)0;
	input.Pos.w = 1.0f;
    output.Pos = mul( input.Pos, World );
    output.Pos = mul( output.Pos, View );
    output.Pos = mul( output.Pos, Projection );
	
    output.Tex = input.Tex;
    
    // Calculate the normal vector against the world matrix only.
    output.Normal = mul(input.Normal, (float3x3)World);
	
    // Normalize the normal vector.
    output.Normal = normalize(output.Normal);

	output.WorldPos = mul(input.Pos, World);
	
	//For specular
	 // Determine the viewing direction based on the position of the camera and the position of the vertex in the world.
    output.ViewDirection = Camera.xyz - output.WorldPos.xyz;
	
    // Normalize the viewing direction vector.
    output.ViewDirection = normalize(output.ViewDirection);
	
	return output;
}
 
 
  //VS ************ FULLSCREEN QUAD **************///
PS_INPUT VS_FULLSCREENQUAD( uint id : SV_VERTEXID ) //:SV_POSITION
{
	PS_INPUT output = (PS_INPUT)0;

    output.Tex = float2(id&1,id>>1); //you can use these for texture coordinates later
	//output.Tex = float2((2-id << 1) & 2, 2-id & 2);
    output.Pos = float4((output.Tex.x-0.5f)*2,-(output.Tex.y-0.5f)*2,0,1);
	
	return output;
}


//************SSAO **********************//
float4 PS_SSAO(PS_INPUT input) : SV_Target
{
	float3 random = normalize( qMap.Sample(samLinear, input.Tex * 4.0).rgb ); 
	
	float depth = txlinearDepthMap.Sample(samLinear, input.Tex).r;
	
	float3 position = float3(input.Tex, depth);
	//float3 normal = normal_from_depth(depth, In.Tex0);
	
	const float2 offset1 = float2(0.0,0.001);
	const float2 offset2 = float2(0.001,0.0);
	
	float depth1 = txlinearDepthMap.Sample(samLinear, input.Tex + offset1).r;//tex2D(DepthTextureSampler, texcoords + offset1).r;
	float depth2 = txlinearDepthMap.Sample(samLinear, input.Tex + offset2).r;//tex2D(DepthTextureSampler, texcoords + offset2).r;
	
	float3 p1 = float3(offset1, depth1 - depth);
	float3 p2 = float3(offset2, depth2 - depth);
	
	float3 normal = cross(p1, p2);
	normal.z = -normal.z;
	
	normal = normalize(normal);
	
	//float3 normal = txnormalMap.Sample(samLinear, input.Tex).rgb;
	
	float radius_depth = radius/depth;
	float occlusion = 0.0;
	for(int i=0; i < samples; i++) {
	
		float3 ray = radius_depth * reflect(sample_sphere[i], random);
		float3 hemi_ray = position + sign(dot(ray,normal)) * ray;
		
		float occ_depth = txlinearDepthMap.Sample(samLinear, saturate(hemi_ray.xy)).r;//tex2D(DepthTextureSampler, saturate(hemi_ray.xy)).r;
		float difference = depth - occ_depth;
		
		occlusion += step(falloff, difference) * (1.0-smoothstep(falloff, area, difference));
	}
	
	float ao = 1.0 - total_strength * occlusion * (1.0 / samples);
	ao = saturate(ao + base);
	
	return float4(ao, ao, ao, 1.0f);
	
	//float3 fragPos = txlinearDepthMap.Sample(samLinear, input.Tex).xyz;
	//float3 normal = txnormalMap.Sample(samLinear, input.Tex).rgb;
	//float3 randomVec = normalize( qMap.Sample(samLinear, input.Tex * noiseScale).rgb ); 
	//
	//float3 tangent = normalize(randomVec - normal * dot(randomVec, normal));
	//float3 bitangent = cross(normal, tangent);
	//float3x3 TBN = float3x3(tangent, bitangent, normal);
    //
	//float occlusion = 0.0;
	//for(int i = 0; i < samples; ++i)
	//{
	//	// get sample position
	//	float3 sample = mul(TBN , sample_sphere[i]); // From tangent to view-space
	//	sample = fragPos + sample * radius; 
    //
	//	float4 offset = float4(sample.xyz, 1.0);
	//	offset = mul(Projection , offset); // from view to clip-space
	//	offset.xyz /= offset.w; // perspective divide
	//	offset.xyz = offset.xyz * 0.5 + 0.5; // transform to range 0.0 - 1.0 
	//	
	//	float sampleDepth = -txnormalMap.Sample(samLinear, offset.xy).w;
	//	
	//	occlusion += (sampleDepth >= sample.z ? 1.0 : 0.0); 
	//	
	//	float rangeCheck = smoothstep(0.0, 1.0, radius / abs(fragPos.z - sampleDepth));
	//	occlusion += (sampleDepth >= sample.z ? 1.0 : 0.0) * rangeCheck; 
	//}  
	//
	//float ao = 1.0 - (occlusion / samples);
	//return float4(occlusion, occlusion, occlusion, 1.0f); 
	//return vMeshColor;
  
	//return txnormalMap.Sample(samLinear, input.Tex);
}

