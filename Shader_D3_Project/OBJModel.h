#ifndef OBJMODEL_H
#define OBJMODEL_H


#include <iostream>
#include <fstream>
using namespace std;
#include <string>
#include <windows.h>
#include <d3d11.h>
#include <d3dx11.h>
#include <d3dcompiler.h>
#include <xnamath.h>
#include "resource.h"
#include <d3dx10.h>

//// include the Direct3D Library file
#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "d3dx11.lib")
#pragma comment (lib, "d3dx10.lib")

//Vertex Data
struct VertexType
{
	D3DXVECTOR3 position;
	D3DXVECTOR2 texture;
	D3DXVECTOR3 normal;
};

class OBJModel {

private:


public:


	OBJModel();
	~OBJModel();
	bool loadOBJ(const char *path);
	
	//void drawOBJ();

	VertexType *vertices;
	unsigned int *indices;

	unsigned int m_vertexCount = 1080;
	unsigned int m_indexCount = 1080;
};


#endif