#include "OBJModel.h"



OBJModel::OBJModel() {}

OBJModel::~OBJModel() {
	delete[] vertices;
	delete[] indices;
}

bool OBJModel::loadOBJ(const char *path) {
	ifstream fin;
	char input;
	int i;


	// Open the model file.
	fin.open(path);

	// If it could not open the file then exit.
	if (fin.fail())
	{
		return false;
	}

	// Read up to the value of vertex count.
	fin.get(input);
	while (input != ':')
	{
		fin.get(input);
	}

	// Read in the vertex count.
	fin >> m_vertexCount;

	// Set the number of indices to be the same as the vertex count.
	m_indexCount = m_vertexCount;

	// Create the model using the vertex count that was read in.
	vertices = new VertexType[m_vertexCount];
	indices = new unsigned int[m_indexCount];

	if (!vertices)
	{
		return false;
	}

	// Read up to the beginning of the data.
	fin.get(input);
	while (input != ':')
	{
		fin.get(input);
	}
	fin.get(input);
	fin.get(input);

	// Read in the vertex data.
	for (unsigned int i = 0; i<m_vertexCount; i++)
	{
		//add vertices
		float x,y,z;
		float u,v;
		float nx,ny,nz;


		fin >> x >> y >> z;
		fin >> u >> v;
		fin >> nx >> ny >> nz;

		vertices[i].position = D3DXVECTOR3(x, y, z);
		vertices[i].texture = D3DXVECTOR2(u, v);
		vertices[i].normal = D3DXVECTOR3(nx, ny, nz);

		indices[i] = i;
	}


	// Close the model file.
	
	fin.close();

	return true;




	// Create the vertex array.
	//vertices = new VertexType[m_vertexCount];
	//if (!vertices)
	//{
	//	return false;
	//}
	//
	//// Create the index array.
	//indices = new unsigned long[m_indexCount];
	//if (!indices)
	//{
	//	return false;
	//}
	//
	//FILE *file = fopen(path, "r");
	////std::ifstream file;
	////file.open(path);
	//if (file == NULL) {
	//	std::cout << "FILE ERROR /n";
	//}
	//char lineHeader[128];
	////read file, convert from string to vector
	//int index = 0;
	//while (fscanf(file, "%s", lineHeader) != EOF) {
	//
	//	//add vertices
	//	D3DXVECTOR3 vertex;
	//	fscanf(file, "%f %f %f ", &vertex.x, &vertex.y, &vertex.z);
	//	vertices[index].position = vertex;
	//	
	//	// add texture vertices
	//	D3DXVECTOR2 uv;
	//	fscanf(file, "%f %f ", &uv.x, &uv.y);
	//	uv.y = 1 - uv.y;
	//	vertices[index].texture = uv;
	//	
	//	// add normals
	//	D3DXVECTOR3 normal;
	//	fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
	//	vertices[index].normal = normal;
	//	
	//	//add face indices
	//	indices[index] = index;
	//	index++;
	//	//else if (strcmp(lineHeader, "f") == 0) {
	//	//	std::string sVertex1, sVertex2, Vertex3;
	//	//	unsigned int uiVertexIndex[3], uiUVIndex[3], uiNormalIndex[3];
	//	//	int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n",
	//	//		&uiVertexIndex[0], &uiUVIndex[0], &uiNormalIndex[0],
	//	//		&uiVertexIndex[1], &uiUVIndex[1], &uiNormalIndex[1],
	//	//		&uiVertexIndex[2], &uiUVIndex[2], &uiNormalIndex[2]);
	//	//	if (matches != 9) {
	//	//		std::cout << "File can't be read";
	//	//	}
	//	//	vuiVertexIndex.push_back(uiVertexIndex[0]);
	//	//	vuiVertexIndex.push_back(uiVertexIndex[1]);
	//	//	vuiVertexIndex.push_back(uiVertexIndex[2]);
	//	//	vuiUVIndex.push_back(uiUVIndex[0]);
	//	//	vuiUVIndex.push_back(uiUVIndex[1]);
	//	//	vuiUVIndex.push_back(uiUVIndex[2]);
	//	//	vuiNormalIndex.push_back(uiNormalIndex[0]);
	//	//	vuiNormalIndex.push_back(uiNormalIndex[1]);
	//	//	vuiNormalIndex.push_back(uiNormalIndex[2]);
	//	//}
	//}
	//
	////changing vectors into set of flm::vec3 for opengl
	////for (unsigned int i = 0; i<vuiVertexIndex.size(); i++) {
	////	unsigned int vertexIndex = vuiVertexIndex[i];
	////	glm::vec3 vertex = vv3temp_vertex[vertexIndex - 1];
	////	out_vertices.push_back(vertex);
	////}
	////for (unsigned int j = 0; j<vuiUVIndex.size(); j++) {
	////	unsigned int uvIndex = vuiUVIndex[j];
	////	sf::Vector2f uv = vv2temp_uvs[uvIndex - 1];
	////	out_uvs.push_back(uv);
	////}
	////for (unsigned int k = 0; k<vuiNormalIndex.size(); k++) {
	////	unsigned int normalIndex = vuiNormalIndex[k];
	////	glm::vec3 normal = vv3temp_normals[normalIndex - 1];
	////	out_normals.push_back(normal);
	////}
	//
}


//glm::vec3 OBJModel::getVertex(int i) {
//	return out_vertices[i];
//}
//
//sf::Vector2f OBJModel::getUV(int i) {
//	return out_uvs[i];
//}
//
//glm::vec3 OBJModel::getNormal(int i) {
//	return out_normals[i];
//}
//
//int OBJModel::getVerSize() {
//	return out_vertices.size();
//}
//int OBJModel::getUVSize() {
//	return out_uvs.size();
//}
//int OBJModel::getNormSize() {
//	return out_normals.size();
//}
//
//void OBJModel::setPos(float xtemp, float ytemp, float ztemp) {
//	x = xtemp;
//	y = ytemp;
//	z = ztemp;
//}
//
//float OBJModel::getPosX() {
//	return x;
//}
//
//float OBJModel::getPosY() {
//	return y;
//}
//
//float OBJModel::getPosZ() {
//	return z;
//}
//
//GLfloat OBJModel::getTex() {
//	return texture;
//}
