
#include <windows.h>
#include <vector>
#include <d3d11.h>
#include <d3dx11.h>
#include <d3dcompiler.h>
#include <xnamath.h>
//#include "resource.h"
#include <stdlib.h>
#include <d3dx10.h>

// include the Direct3D Library file
#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "d3dx11.lib")
#pragma comment (lib, "d3dx10.lib")

#include "Mesh.h"






//--------------------------------------------------------------------------------------
// Global Variables
//--------------------------------------------------------------------------------------
HINSTANCE                           g_hInst = NULL;
HWND                                g_hWnd = NULL;
D3D_DRIVER_TYPE                     g_driverType = D3D_DRIVER_TYPE_NULL;
D3D_FEATURE_LEVEL                   g_featureLevel = D3D_FEATURE_LEVEL_11_0;
ID3D11Device*                       g_pd3dDevice = NULL;
ID3D11DeviceContext*                g_pImmediateContext = NULL;
IDXGISwapChain*                     g_pSwapChain = NULL;
ID3D11RenderTargetView*             g_pRenderTargetView = NULL;
ID3D11Texture2D*                    g_pDepthStencil = NULL;
ID3D11DepthStencilView*             g_pDepthStencilView = NULL;
ID3D11VertexShader*                 g_pVertexShader = NULL;
ID3D11PixelShader*                  g_pPixelShader = NULL;
ID3D11InputLayout*                  g_pVertexLayout = NULL;
ID3D11Buffer*                       g_pVertexBuffer = NULL;
ID3D11Buffer*                       g_pIndexBuffer = NULL;
ID3D11Buffer*                       g_pCBNeverChanges = NULL;
ID3D11Buffer*                       g_pCBChangeOnResize = NULL;
ID3D11Buffer*                       g_pCBChangesEveryFrame = NULL;
ID3D11ShaderResourceView*           g_pTextureRV = NULL;
ID3D11SamplerState*                 g_pSamplerLinear = NULL;
XMMATRIX                            g_World;
XMMATRIX                            g_View;
XMMATRIX                            g_Projection;
XMFLOAT4                            g_vMeshColor( 0.7f, 0.7f, 0.7f, 1.0f );
int									Screen_Width = 1920;
int									Screen_Height = 1080;
D3D11_VIEWPORT						vp;

//shadow map resources
ID3D11SamplerState*                 pSamplerLinear = NULL;
ID3D11Buffer*                       pCBNeverChanges = NULL;
ID3D11Buffer*                       pCBChangeOnResize = NULL;
ID3D11Buffer*                       pCBChangesEveryFrame = NULL;
ID3D11VertexShader*                 pVertexShader = NULL;
ID3D11PixelShader*                  pPixelShader = NULL;
ID3D11Texture2D*					pShadowMap	= NULL;
ID3D11DepthStencilView*				pShadowMapDepthView = NULL;
ID3D11RenderTargetView*             pShadowRenderTargetView = NULL;
ID3D11ShaderResourceView*			pShadowMapSRView = NULL;
D3D11_VIEWPORT						shadowMapViewport;
float								shadowMapBias = 0.0005f;


D3D11_VIEWPORT						fullScreenViewport;

D3D11_VIEWPORT						phonnViewPort;
ID3D11Texture2D*					phonnMap = NULL;
ID3D11VertexShader*                 phongVertexShader = NULL;
ID3D11PixelShader*                  phongPixelShader = NULL;
ID3D11RenderTargetView*             phongRenderTarget = NULL;
ID3D11DepthStencilView*             phongDepthStencilView = NULL;
ID3D11Texture2D*                    phongDepthStencil = NULL;
ID3D11ShaderResourceView*			phonnMapRSView = NULL;
//D3D11_VIEWPORT						fullScreenViewport;

//shadow map resources
ID3D11VertexShader*                 toonVertexShader = NULL;
ID3D11PixelShader*                  toonPixelShader = NULL;
ID3D11VertexShader*                 outlineVertexShader = NULL;
ID3D11PixelShader*                  outlinePixelShader = NULL;
ID3D11Texture2D*					toonMap = NULL;
ID3D11DepthStencilView*				toonMapDepthView = NULL;
ID3D11RenderTargetView*             toonRenderTargetView = NULL;
ID3D11ShaderResourceView*			toonMapSRView = NULL;
D3D11_VIEWPORT						toonViewPort;
ID3D11ShaderResourceView*           qMap = NULL;

//Normal buffer
ID3D11VertexShader*                 normalVertexShader = NULL;
ID3D11PixelShader*                  normalPixelShader = NULL;
ID3D11Texture2D*					normalMap = NULL;
ID3D11DepthStencilView*				normalMapDepthView = NULL;
ID3D11RenderTargetView*             normalRenderTargetView = NULL;
ID3D11ShaderResourceView*			normalMapSRView = NULL;
//D3D11_VIEWPORT						normalViewPort;

//Normal buffer
ID3D11VertexShader*                 depthVertexShader = NULL;
ID3D11PixelShader*                  depthPixelShader = NULL;
ID3D11Texture2D*					depthMap = NULL;
ID3D11DepthStencilView*				depthMapDepthView = NULL;
ID3D11RenderTargetView*             depthRenderTargetView = NULL;
ID3D11ShaderResourceView*			depthMapSRView = NULL;

//Normal buffer
ID3D11VertexShader*                 ssaoVertexShader = NULL;
ID3D11PixelShader*                  ssaoPixelShader = NULL;
ID3D11Texture2D*					ssaoMap = NULL;
ID3D11DepthStencilView*				ssaoMapDepthView = NULL;
ID3D11RenderTargetView*             ssaoRenderTargetView = NULL;
ID3D11ShaderResourceView*			ssaoMapSRView = NULL;



//OBJModel obj;
//OBJModel objects[5];

std::vector<Mesh> meshes;
Mesh temp;

//--------------------------------------------------------------------------------------
// Forward declarations
//--------------------------------------------------------------------------------------
HRESULT InitWindow( HINSTANCE hInstance, int nCmdShow );
HRESULT InitDevice();
HRESULT shadowMapInit();
HRESULT ShadowMapSetup();
HRESULT phonnBlinnInit();
HRESULT toonShader();
HRESULT normalBuffer();
HRESULT depthBuffer();
HRESULT ssaoInit();
void ShadowMapShadersInit();
void CleanupDevice();
void DisableDepthTest();
void EnableDepthTest();
LRESULT CALLBACK    WndProc( HWND, UINT, WPARAM, LPARAM );
void Render();



//--------------------------------------------------------------------------------------
// Entry point to the program. Initializes everything and goes into a message processing 
// loop. Idle time is used to render the scene.
//--------------------------------------------------------------------------------------
int WINAPI wWinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow )
{
    UNREFERENCED_PARAMETER( hPrevInstance );
    UNREFERENCED_PARAMETER( lpCmdLine );

    if( FAILED( InitWindow( hInstance, nCmdShow ) ) )
        return 0;

    if( FAILED( InitDevice() ) )
    {
        CleanupDevice();
        return 0;
    }

    // Main message loop
    MSG msg = {0};
    while( WM_QUIT != msg.message )
    {
        if( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) )
        {
            TranslateMessage( &msg );
            DispatchMessage( &msg );
        }
        else
        {
            Render();
        }
    }

    CleanupDevice();

    return ( int )msg.wParam;
}


//--------------------------------------------------------------------------------------
// Register class and create window
//--------------------------------------------------------------------------------------
HRESULT InitWindow( HINSTANCE hInstance, int nCmdShow )
{
    // Register class
    WNDCLASSEX wcex;
    wcex.cbSize = sizeof( WNDCLASSEX );
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon( hInstance, ( LPCTSTR )IDI_TUTORIAL1 );
    wcex.hCursor = LoadCursor( NULL, IDC_ARROW );
    wcex.hbrBackground = ( HBRUSH )( COLOR_WINDOW + 1 ); 
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = L"WindowClass";
    wcex.hIconSm = LoadIcon( wcex.hInstance, ( LPCTSTR )IDI_TUTORIAL1 );
    if( !RegisterClassEx( &wcex ) )
        return E_FAIL;

    // Create window
    g_hInst = hInstance;
    RECT rc = { 0, 0, Screen_Width, Screen_Height };
    AdjustWindowRect( &rc, WS_OVERLAPPEDWINDOW, FALSE );
    g_hWnd = CreateWindow( L"WindowClass", L"Super Amazing DirectX Shaders", WS_OVERLAPPEDWINDOW,
                           CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance,
                           NULL );
    if( !g_hWnd )
        return E_FAIL;

    ShowWindow( g_hWnd, nCmdShow );

    return S_OK;
}


//--------------------------------------------------------------------------------------
// Helper for compiling shaders with D3DX11
//--------------------------------------------------------------------------------------
HRESULT CompileShaderFromFile( WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut )
{
    HRESULT hr = S_OK;

    DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
    // Setting this flag improves the shader debugging experience, but still allows 
    // the shaders to be optimized and to run exactly the way they will run in 
    // the release configuration of this program.
    dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

    ID3DBlob* pErrorBlob;
    hr = D3DX11CompileFromFile( szFileName, NULL, NULL, szEntryPoint, szShaderModel, 
        dwShaderFlags, 0, NULL, ppBlobOut, &pErrorBlob, NULL );
    if( FAILED(hr) )
    {
        if( pErrorBlob != NULL )
            OutputDebugStringA( (char*)pErrorBlob->GetBufferPointer() );
        if( pErrorBlob ) pErrorBlob->Release();
        return hr;
    }
    if( pErrorBlob ) pErrorBlob->Release();

    return S_OK;
}


//--------------------------------------------------------------------------------------
// Create Direct3D device and swap chain
//--------------------------------------------------------------------------------------
HRESULT InitDevice()
{
    HRESULT hr = S_OK;

	


	

    RECT rc;
    GetClientRect( g_hWnd, &rc );
    UINT width = rc.right - rc.left;
    UINT height = rc.bottom - rc.top;

    UINT createDeviceFlags = 0;
#ifdef _DEBUG
    createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

    D3D_DRIVER_TYPE driverTypes[] =
    {
        D3D_DRIVER_TYPE_HARDWARE,
        D3D_DRIVER_TYPE_WARP,
        D3D_DRIVER_TYPE_REFERENCE,
    };
    UINT numDriverTypes = ARRAYSIZE( driverTypes );

    D3D_FEATURE_LEVEL featureLevels[] =
    {
        D3D_FEATURE_LEVEL_11_0,
        D3D_FEATURE_LEVEL_10_1,
        D3D_FEATURE_LEVEL_10_0,
    };
    UINT numFeatureLevels = ARRAYSIZE( featureLevels );

    DXGI_SWAP_CHAIN_DESC sd;
    ZeroMemory( &sd, sizeof( sd ) );
    sd.BufferCount = 1;
    sd.BufferDesc.Width = width;
    sd.BufferDesc.Height = height;
    sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.BufferDesc.RefreshRate.Numerator = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.OutputWindow = g_hWnd;
    sd.SampleDesc.Count = 1;
    sd.SampleDesc.Quality = 0;
    sd.Windowed = TRUE;

    for( UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++ )
    {
        g_driverType = driverTypes[driverTypeIndex];
        hr = D3D11CreateDeviceAndSwapChain( NULL, g_driverType, NULL, createDeviceFlags, featureLevels, numFeatureLevels,
                                            D3D11_SDK_VERSION, &sd, &g_pSwapChain, &g_pd3dDevice, &g_featureLevel, &g_pImmediateContext );
        if( SUCCEEDED( hr ) )
            break;
    }
    if( FAILED( hr ) )
        return hr;

    // Create a render target view
    ID3D11Texture2D* pBackBuffer = NULL;
    hr = g_pSwapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), ( LPVOID* )&pBackBuffer );
    if( FAILED( hr ) )
        return hr;

    hr = g_pd3dDevice->CreateRenderTargetView( pBackBuffer, NULL, &g_pRenderTargetView );
    pBackBuffer->Release();
    if( FAILED( hr ) )
        return hr;


	//ShadowMap
	//Shadow Mapping RENDER TARGET ***********
	if (FAILED(shadowMapInit()))
	{
		CleanupDevice();
		return 0;
	}


    // Create depth stencil texture
    D3D11_TEXTURE2D_DESC descDepth;
    ZeroMemory( &descDepth, sizeof(descDepth) );
    descDepth.Width = width;
    descDepth.Height = height;
    descDepth.MipLevels = 1;
    descDepth.ArraySize = 1;
    descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    descDepth.SampleDesc.Count = 1;
    descDepth.SampleDesc.Quality = 0;
    descDepth.Usage = D3D11_USAGE_DEFAULT;
    descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    descDepth.CPUAccessFlags = 0;
    descDepth.MiscFlags = 0;
    hr = g_pd3dDevice->CreateTexture2D( &descDepth, NULL, &g_pDepthStencil );
    if( FAILED( hr ) )
        return hr;

    // Create the depth stencil view
    D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
    ZeroMemory( &descDSV, sizeof(descDSV) );
    descDSV.Format = descDepth.Format;
    descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    descDSV.Texture2D.MipSlice = 0;
    hr = g_pd3dDevice->CreateDepthStencilView( g_pDepthStencil, &descDSV, &g_pDepthStencilView );
    if( FAILED( hr ) )
        return hr;

	EnableDepthTest();


    //g_pImmediateContext->OMSetRenderTargets( 1, &g_pRenderTargetView, g_pDepthStencilView );


	fullScreenViewport.Width = (FLOAT)width;
	fullScreenViewport.Height = (FLOAT)height;
	fullScreenViewport.MinDepth = 0.0f;
	fullScreenViewport.MaxDepth = 1.0f;
	fullScreenViewport.TopLeftX = 0;
	fullScreenViewport.TopLeftY = 0;
	g_pImmediateContext->RSSetViewports(1, &fullScreenViewport);


    // Setup the viewport
    //D3D11_VIEWPORT vp;
    vp.Width = (FLOAT)width/2;
    vp.Height = (FLOAT)height/2;
    vp.MinDepth = 0.0f;
    vp.MaxDepth = 1.0f;
    vp.TopLeftX = 0;
    vp.TopLeftY = 0;
    g_pImmediateContext->RSSetViewports( 1, &vp );


	phonnViewPort.Width = (FLOAT)width / 2;
	phonnViewPort.Height = (FLOAT)height / 2;
	phonnViewPort.MinDepth = 0.0f;
	phonnViewPort.MaxDepth = 1.0f;
	phonnViewPort.TopLeftX = (FLOAT)width / 2;
	phonnViewPort.TopLeftY = (FLOAT)height / 2;
	g_pImmediateContext->RSSetViewports(1, &phonnViewPort);

	toonViewPort.Width = (FLOAT)width / 2;
	toonViewPort.Height = (FLOAT)height / 2;
	toonViewPort.MinDepth = 0.0f;
	toonViewPort.MaxDepth = 1.0f;
	toonViewPort.TopLeftX = 0;
	toonViewPort.TopLeftY = (FLOAT)height / 2;
	g_pImmediateContext->RSSetViewports(1, &toonViewPort);


    // Compile the vertex shader
    ID3DBlob* pVSBlob = NULL;
    hr = CompileShaderFromFile( L"Shaders.fx", "VS", "vs_4_0", &pVSBlob );
    if( FAILED( hr ) )
    {
        MessageBox( NULL,
                    L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK );
        return hr;
    }

    // Create the vertex shader
    hr = g_pd3dDevice->CreateVertexShader( pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &g_pVertexShader );
    if( FAILED( hr ) )
    {    
        pVSBlob->Release();
        return hr;
    }

    // Define the input layout
    D3D11_INPUT_ELEMENT_DESC layout[]
    {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",	0,	DXGI_FORMAT_R32G32B32_FLOAT, 0 , 20, D3D11_INPUT_PER_VERTEX_DATA, 0 }
    };
    UINT numElements = ARRAYSIZE( layout );


    // Create the input layout
    hr = g_pd3dDevice->CreateInputLayout( layout, numElements, pVSBlob->GetBufferPointer(),
                                          pVSBlob->GetBufferSize(), &g_pVertexLayout );
    pVSBlob->Release();
    if( FAILED( hr ) )
        return hr;

    // Set the input layout
    g_pImmediateContext->IASetInputLayout( g_pVertexLayout );

    // Compile the pixel shader
    ID3DBlob* pPSBlob = NULL;
    hr = CompileShaderFromFile( L"Shaders.fx", "PS", "ps_4_0", &pPSBlob );
    if( FAILED( hr ) )
    {
        MessageBox( NULL,
                    L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK );
        return hr;
    }

    // Create the pixel shader
    hr = g_pd3dDevice->CreatePixelShader( pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &g_pPixelShader );
    pPSBlob->Release();
    if( FAILED( hr ) )
        return hr;

	//Load models
	//obj.loadOBJ("model.txt");
	//objects[0].loadOBJ("model1.txt");
	//objects[1].loadOBJ("model2.txt");
	//objects[2].loadOBJ("model3.txt");
	//objects[3].loadOBJ("model4.txt");
	//objects[4].loadOBJ("model5.txt");

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;

	meshes.push_back(temp);
	meshes.push_back(temp);
	meshes.push_back(temp);
	meshes.push_back(temp);
	meshes.push_back(temp);
	//meshes.push_back(temp);
	meshes[0].CreateMesh(g_pd3dDevice, "model1.txt");
	meshes[1].CreateMesh(g_pd3dDevice, "model2.txt");
	meshes[2].CreateMesh(g_pd3dDevice, "model3.txt");
	meshes[3].CreateMesh(g_pd3dDevice, "model4.txt");
	meshes[4].CreateMesh(g_pd3dDevice, "model5.txt");

	//meshes[5].CreateMesh(g_pd3dDevice, "model6.txt");





    //bd.ByteWidth = sizeof( VertexType ) * obj.m_vertexCount;
	////bd.ByteWidth = sizeof(SimpleVertex) * 24;
    //bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    //bd.CPUAccessFlags = 0;
    //D3D11_SUBRESOURCE_DATA InitData;
    //ZeroMemory( &InitData, sizeof(InitData) );
    //InitData.pSysMem = obj.vertices;
	//InitData.SysMemPitch = 0;
	//InitData.SysMemSlicePitch = 0;
    //hr = g_pd3dDevice->CreateBuffer( &bd, &InitData, &g_pVertexBuffer );
    //if( FAILED( hr ) )
    //    return hr;
	//
	//
	//
	//
    ////bd.Usage = D3D11_USAGE_DEFAULT;
    ////bd.ByteWidth = sizeof( unsigned int ) * obj.m_indexCount;
	//////bd.ByteWidth = sizeof(WORD) * 36;
    ////bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
    ////bd.CPUAccessFlags = 0;
    //InitData.pSysMem = obj.indices;
	//InitData.SysMemPitch = 0;
	//InitData.SysMemSlicePitch = 0;
	//
	//// Set up the description of the static index buffer.
	//bd.Usage = D3D11_USAGE_DEFAULT;
	//bd.ByteWidth = sizeof(unsigned int) * obj.m_indexCount;
	//bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	//bd.CPUAccessFlags = 0;
	//bd.MiscFlags = 0;
	//bd.StructureByteStride = 0;
	//
    //hr = g_pd3dDevice->CreateBuffer( &bd, &InitData, &g_pIndexBuffer );
    //if( FAILED( hr ) )
    //    return hr;


	//// Set vertex buffer
	//UINT stride = sizeof(VertexType);
	////UINT stride = sizeof(SimpleVertex);
	//UINT offset = 0;
	//g_pImmediateContext->IASetVertexBuffers(0, 1, &g_pVertexBuffer, &stride, &offset);
    //// Set index buffer
    //g_pImmediateContext->IASetIndexBuffer( g_pIndexBuffer, DXGI_FORMAT_R32_UINT, 0 );
	////g_pImmediateContext->IASetIndexBuffer(g_pIndexBuffer, DXGI_FORMAT_R16_UINT, 0);
	//
    //// Set primitive topology
    //g_pImmediateContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

    // Create the constant buffers
    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = sizeof(CBNeverChanges);
    bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    bd.CPUAccessFlags = 0;
    hr = g_pd3dDevice->CreateBuffer( &bd, NULL, &g_pCBNeverChanges );
    if( FAILED( hr ) )
        return hr;
    
    bd.ByteWidth = sizeof(CBChangeOnResize);
    hr = g_pd3dDevice->CreateBuffer( &bd, NULL, &g_pCBChangeOnResize );
    if( FAILED( hr ) )
        return hr;
    
    bd.ByteWidth = sizeof(CBChangesEveryFrame);
    hr = g_pd3dDevice->CreateBuffer( &bd, NULL, &g_pCBChangesEveryFrame );
    if( FAILED( hr ) )
        return hr;

    // Load the Texture  //metal001.dds
    hr = D3DX11CreateShaderResourceViewFromFile( g_pd3dDevice, L"metal001.dds", NULL, NULL, &g_pTextureRV, NULL );
    if( FAILED( hr ) )
        return hr;
	//hr = D3DX11CreateShaderResourceViewFromFile(g_pd3dDevice, L"ShadowTest.png", NULL, NULL, &pShadowMapSRView, NULL);
	//if (FAILED(hr))
	//	return hr;

	g_pImmediateContext->PSSetShaderResources(1, 1, &pShadowMapSRView);

	hr = D3DX11CreateShaderResourceViewFromFile(g_pd3dDevice, L"PerlinNoiseFractal.png", NULL, NULL, &qMap, NULL);
	if (FAILED(hr))
		return hr;

	g_pImmediateContext->PSSetShaderResources(3, 1, &normalMapSRView);
	g_pImmediateContext->PSSetShaderResources(4, 1, &depthMapSRView);

    // Create the sample state
    D3D11_SAMPLER_DESC sampDesc;
    ZeroMemory( &sampDesc, sizeof(sampDesc) );
    sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.MipLODBias = 0.0f;
	sampDesc.MaxAnisotropy = 1;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	sampDesc.BorderColor[0] = 0;
	sampDesc.BorderColor[1] = 0;
	sampDesc.BorderColor[2] = 0;
	sampDesc.BorderColor[3] = 0;
    sampDesc.MinLOD = 0;
    sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
    hr = g_pd3dDevice->CreateSamplerState( &sampDesc, &g_pSamplerLinear );
    if( FAILED( hr ) )
        return hr;

    // Initialize the world matrices
    g_World = XMMatrixIdentity();

    // Initialize the view matrix
	
    XMVECTOR Eye = XMVectorSet( 0.0f, 10.0f, -20.0f, 0.0f );
    XMVECTOR At = XMVectorSet( 0.0f, 1.0f, 0.0f, 0.0f );
    XMVECTOR Up = XMVectorSet( 0.0f, 1.0f, 0.0f, 0.0f );
    g_View = XMMatrixLookAtLH( Eye, At, Up );



	//Light and stuff
	CBNeverChanges cbNeverChanges;
	cbNeverChanges.BiasMat = XMMATRIX(
		0.5f, 0.0f, 0.0f, 0.5f,
		0.0f, 0.5f, 0.0f, 0.5f,
		0.0f, 0.0f, 0.5f, 0.5f,
		0.0f, 0.0f, 0.0f, 1.0f
		);

	

	cbNeverChanges.LightPos = XMFLOAT4(15.0f, 10.0f, 0.0f, 1.0f);
	XMVECTOR lp = XMVectorSet(15.0f, 10.0f, 0.0f, 1.0f);
	XMMATRIX lightProj = XMMatrixPerspectiveFovLH(XM_PIDIV4, width / (FLOAT)height, 0.01f, 1000.0f);
	XMMATRIX vm = XMMatrixLookAtLH(lp, At, Up);

	cbNeverChanges.LightProj = XMMatrixTranspose(lightProj);//XMMatrixMultiply(cbNeverChanges.LightProj, vm);
	cbNeverChanges.LightMVP = XMMatrixTranspose(vm);
	cbNeverChanges.mView = XMMatrixTranspose(g_View);

	g_pImmediateContext->UpdateSubresource(g_pCBNeverChanges, 0, NULL, &cbNeverChanges, 0, 0);

	// Initialize the projection matrix
	g_Projection = XMMatrixPerspectiveFovLH(XM_PIDIV4, width / (FLOAT)height, 0.01f, 1000.0f);

	CBChangeOnResize cbChangesOnResize;
	cbChangesOnResize.mProjection = XMMatrixTranspose(g_Projection);
	g_pImmediateContext->UpdateSubresource(g_pCBChangeOnResize, 0, NULL, &cbChangesOnResize, 0, 0);

	if (FAILED(toonShader()))
	{
		CleanupDevice();
		return 0;
	}

	if (FAILED(phonnBlinnInit()))
	{
		CleanupDevice();
		return 0;
	}

	//ShadowMap
	//Shadow Mapping setup TARGET ***********
	if (FAILED(ShadowMapSetup()))
	{
		CleanupDevice();
		return 0;
	}

	
		if (FAILED(normalBuffer()))
		{
			CleanupDevice();
			return 0;
		}

		if (FAILED(ssaoInit()))
		{
			CleanupDevice();
			return 0;
		}

	if (FAILED(depthBuffer()))
	{
		CleanupDevice();
		return 0;
	}

			

    return S_OK;
}

HRESULT phonnBlinnInit() {

	RECT rc;
	GetClientRect(g_hWnd, &rc);
	UINT width = rc.right - rc.left;
	UINT height = rc.bottom - rc.top;

	HRESULT phonnHR = S_OK;


	// Create a render target view
	ID3D11Texture2D* pBackBuffer = NULL;
	phonnHR = g_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
	if (FAILED(phonnHR))
		return phonnHR;

	phonnHR = g_pd3dDevice->CreateRenderTargetView(pBackBuffer, NULL, &phongRenderTarget);
	pBackBuffer->Release();
	if (FAILED(phonnHR))
		return phonnHR;


	//Render Specular
	g_pImmediateContext->RSSetViewports(1, &phonnViewPort);


	// Compile the vertex shader
	ID3DBlob* pVSBlob = NULL;
	phonnHR = CompileShaderFromFile(L"Shaders.fx", "VS_Phong", "vs_4_0", &pVSBlob);
	if (FAILED(phonnHR))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return phonnHR;
	}

	// Create the vertex shader
	phonnHR = g_pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &phongVertexShader);
	if (FAILED(phonnHR))
	{
		pVSBlob->Release();
		return phonnHR;
	}

	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC layout[]
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",	0,	DXGI_FORMAT_R32G32B32_FLOAT, 0 , 20, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT numElements = ARRAYSIZE(layout);


	// Create the input layout
	phonnHR = g_pd3dDevice->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(),
		pVSBlob->GetBufferSize(), &g_pVertexLayout);
	pVSBlob->Release();
	if (FAILED(phonnHR))
		return phonnHR;

	// Set the input layout
	g_pImmediateContext->IASetInputLayout(g_pVertexLayout);

	// Compile the pixel shader
	ID3DBlob* pPSBlob = NULL;
	phonnHR = CompileShaderFromFile(L"Shaders.fx", "PS_Phong", "ps_4_0", &pPSBlob);
	if (FAILED(phonnHR))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return phonnHR;
	}

	// Create the pixel shader
	phonnHR = g_pd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &phongPixelShader);
	pPSBlob->Release();
	if (FAILED(phonnHR))
		return phonnHR;


	return S_OK;
}

//Shadow Map Init
HRESULT shadowMapInit() {

	RECT rc;
	GetClientRect(g_hWnd, &rc);
	UINT width = rc.right - rc.left;
	UINT height = rc.bottom - rc.top;


	//Shadow Mapping RENDER TARGET ***********
	HRESULT shadowrenderTarget = S_OK;

	//create shadow map texture desc
	D3D11_TEXTURE2D_DESC texDesc;
	texDesc.Width = width;
	texDesc.Height = height;
	texDesc.MipLevels = 1;
	texDesc.ArraySize = 1;
	texDesc.Format = DXGI_FORMAT_R32_TYPELESS;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Usage = D3D11_USAGE_DEFAULT;
	texDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = 0;





	//create texture and depth/resource views
	//shadowrenderTarget = g_pd3dDevice->CreateTexture2D(&texDesc, NULL, &pShadowMap);
	//if (FAILED(shadowrenderTarget))
	//	return shadowrenderTarget;

	//// STENCIL BUFFER
	//// SHADOW MAPPING TEXTURE
	// Create depth stencil texture
	//D3D11_TEXTURE2D_DESC descDepth;
	//ZeroMemory(&descDepth, sizeof(descDepth));
	//descDepth.Width = width;
	//descDepth.Height = height;
	//descDepth.Format = DXGI_FORMAT_R32_TYPELESS;
	//descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	// Create depth stencil texture
	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory(&descDepth, sizeof(descDepth));
	descDepth.Width = width;
	descDepth.Height = height;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	descDepth.SampleDesc.Count = 1;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;

	// Create the depth stencil view desc
	//D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	//descDSV.Format = texDesc.Format;
	//descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	//descDSV.Texture2D.MipSlice = 0;
	// Create the depth stencil view
	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));
	descDSV.Format = descDepth.Format;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;

	//create shader resource view desc
	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	ZeroMemory(&descDSV, sizeof(srvDesc));
	srvDesc.Format = descDepth.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = descDepth.MipLevels;
	srvDesc.Texture2D.MostDetailedMip = 0;
	
	
	//create texture and depth/resource views
	shadowrenderTarget = g_pd3dDevice->CreateTexture2D(&descDepth, NULL, &pShadowMap);
	if (FAILED(shadowrenderTarget))
		return shadowrenderTarget;

	D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
	renderTargetViewDesc.Format = descDepth.Format;
	renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	renderTargetViewDesc.Texture2D.MipSlice = 0;

	shadowrenderTarget = g_pd3dDevice->CreateRenderTargetView(pShadowMap, &renderTargetViewDesc, &pShadowRenderTargetView);
	if (FAILED(shadowrenderTarget))
		return shadowrenderTarget;

	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
	shaderResourceViewDesc.Format = descDepth.Format;
	shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
	shaderResourceViewDesc.Texture2D.MipLevels = 1;


	shadowrenderTarget = g_pd3dDevice->CreateShaderResourceView(pShadowMap, &shaderResourceViewDesc, &pShadowMapSRView);
	if (FAILED(shadowrenderTarget))
		return shadowrenderTarget;

	//shadowrenderTarget = g_pd3dDevice->CreateDepthStencilView(pShadowMap, &descDSV, &pShadowMapDepthView);
	//if (FAILED(shadowrenderTarget))
	//	return shadowrenderTarget;
	//shadowrenderTarget = g_pd3dDevice->CreateShaderResourceView(pShadowMap, &srvDesc, &pShadowMapSRView);
	//if (FAILED(shadowrenderTarget))
	//	return shadowrenderTarget;

	//g_pImmediateContext->OMSetRenderTargets(1, &pShadowRenderTargetView, pShadowMapDepthView);
	
	//shadowrenderTarget = g_pd3dDevice->tTexture(0, pShadowRenderTargetView);

	//
	///*******************************************************************
	//* Set up Viewports
	//********************************************************************/
	//
	shadowMapViewport.Width = (FLOAT)width/2;
	shadowMapViewport.Height = (FLOAT)height/2;
	shadowMapViewport.MinDepth = 0.0f;
	shadowMapViewport.MaxDepth = 1.0f;
	shadowMapViewport.TopLeftX = (FLOAT)width / 2;
	shadowMapViewport.TopLeftY = 0;
	g_pImmediateContext->RSSetViewports(1, &shadowMapViewport);


	//Shadow Mapping RENDER TARGET ***********
	//shadowMap Vertexshader
	ID3DBlob* VS_depthBlob = NULL;
	shadowrenderTarget = CompileShaderFromFile(L"Shaders.fx", "VS_Depth", "vs_4_0", &VS_depthBlob);
	if (FAILED(shadowrenderTarget))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return shadowrenderTarget;
	}
	
	// Create the vertex shader
	shadowrenderTarget = g_pd3dDevice->CreateVertexShader(VS_depthBlob->GetBufferPointer(), VS_depthBlob->GetBufferSize(), NULL, &pVertexShader);
	if (FAILED(shadowrenderTarget))
	{
		VS_depthBlob->Release();
		return shadowrenderTarget;
	}

	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC layout[]
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",	0,	DXGI_FORMAT_R32G32B32_FLOAT, 0 , 20, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT numElements = ARRAYSIZE(layout);


	// Create the input layout
	shadowrenderTarget = g_pd3dDevice->CreateInputLayout(layout, numElements, VS_depthBlob->GetBufferPointer(),
		VS_depthBlob->GetBufferSize(), &g_pVertexLayout);
	VS_depthBlob->Release();
	if (FAILED(shadowrenderTarget))
		return shadowrenderTarget;

	// Set the input layout
	g_pImmediateContext->IASetInputLayout(g_pVertexLayout);

	// Compile the pixel shader
	ID3DBlob* PS_depthBlob = NULL;
	shadowrenderTarget = CompileShaderFromFile(L"Shaders.fx", "PS_Depth", "ps_4_0", &PS_depthBlob);
	if (FAILED(shadowrenderTarget))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return shadowrenderTarget;
	}

	// Create the pixel shader
	shadowrenderTarget = g_pd3dDevice->CreatePixelShader(PS_depthBlob->GetBufferPointer(), PS_depthBlob->GetBufferSize(), NULL, &pPixelShader);
	PS_depthBlob->Release();
	if (FAILED(shadowrenderTarget))
		return shadowrenderTarget;



	return S_OK;
}

HRESULT ShadowMapSetup() {
	HRESULT hr = S_OK;

	RECT rc;
	GetClientRect(g_hWnd, &rc);
	UINT width = rc.right - rc.left;
	UINT height = rc.bottom - rc.top;



	// Load the Texture  //metal001.dds
	//hr = D3DX11CreateShaderResourceViewFromFile(g_pd3dDevice, L"metal001.dds", NULL, NULL, &g_pTextureRV, NULL);
	//if (FAILED(hr))
	//	return hr;


	//g_pImmediateContext->PSSetShaderResources(1, 1, &pShadowMapSRView);

	

	// Create the sample state
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.MipLODBias = 0.0f;
	sampDesc.MaxAnisotropy = 1;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	sampDesc.BorderColor[0] = 0;
	sampDesc.BorderColor[1] = 0;
	sampDesc.BorderColor[2] = 0;
	sampDesc.BorderColor[3] = 0;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	hr = g_pd3dDevice->CreateSamplerState(&sampDesc, &pSamplerLinear);
	if (FAILED(hr))
		return hr;

	// Initialize the world matrices
	g_World = XMMatrixIdentity();

	// Initialize the view matrix

	XMVECTOR Eye = XMVectorSet(0.0f, 10.0f, -20.0f, 0.0f);
	XMVECTOR At = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	XMVECTOR Up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	g_View = XMMatrixLookAtLH(Eye, At, Up);


	//Light and stuff
	CBNeverChanges cbNeverChanges;
	cbNeverChanges.BiasMat = XMMATRIX(
		0.5f, 0.0f, 0.0f, 0.5f,
		0.0f, 0.5f, 0.0f, 0.5f,
		0.0f, 0.0f, 0.5f, 0.5f,
		0.0f, 0.0f, 0.0f, 1.0f
		);
	cbNeverChanges.LightPos = XMFLOAT4(15.0f, 10.0f, 0.0f, 1.0f);
	XMVECTOR lp = XMVectorSet(15.0f, 10.0f, 0.0f, 1.0f);
	XMMATRIX lightProj = XMMatrixPerspectiveFovLH(XM_PIDIV4, width / (FLOAT)height, 0.01f, 1000.0f);
	XMMATRIX vm = XMMatrixLookAtLH(lp, At, Up);
	//cbNeverChanges.LightMVP = vm;
	//cbNeverChanges.LightMVP = XMMatrixMultiply(cbNeverChanges.LightProj, vm);
	cbNeverChanges.LightMVP = XMMatrixTranspose(vm);
	cbNeverChanges.LightProj = XMMatrixTranspose(lightProj);
	cbNeverChanges.mView = XMMatrixTranspose(g_View);

	g_pImmediateContext->UpdateSubresource(g_pCBNeverChanges, 0, NULL, &cbNeverChanges, 0, 0);

	// Initialize the projection matrix
	g_Projection = XMMatrixPerspectiveFovLH(XM_PIDIV4, width / (FLOAT)height, 0.01f, 1000.0f);

	CBChangeOnResize cbChangesOnResize;
	cbChangesOnResize.mProjection = XMMatrixTranspose(g_Projection);
	g_pImmediateContext->UpdateSubresource(g_pCBChangeOnResize, 0, NULL, &cbChangesOnResize, 0, 0);

	return S_OK;
}


HRESULT toonShader() {
	RECT rc;
	GetClientRect(g_hWnd, &rc);
	UINT width = rc.right - rc.left;
	UINT height = rc.bottom - rc.top;

	HRESULT phonnHR = S_OK;


	// Create a render target view
	ID3D11Texture2D* pBackBuffer = NULL;
	phonnHR = g_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
	if (FAILED(phonnHR))
		return phonnHR;

	phonnHR = g_pd3dDevice->CreateRenderTargetView(pBackBuffer, NULL, &toonRenderTargetView);
	pBackBuffer->Release();
	if (FAILED(phonnHR))
		return phonnHR;


	//Render Specular
	g_pImmediateContext->RSSetViewports(1, &toonViewPort);


	// Compile the vertex shader
	ID3DBlob* pVSBlob = NULL;
	//phonnHR = CompileShaderFromFile(L"Shaders.fx", "VS_OutlineShader", "vs_4_0", &pVSBlob);
	//if (FAILED(phonnHR))
	//{
	//	MessageBox(NULL,
	//		L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
	//	return phonnHR;
	//}
	//
	//// Create the vertex shader
	//phonnHR = g_pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &outlineVertexShader);
	//if (FAILED(phonnHR))
	//{
	//	pVSBlob->Release();
	//	return phonnHR;
	//}

	// Compile the vertex shader
	pVSBlob = NULL;
	phonnHR = CompileShaderFromFile(L"Shaders.fx", "VS_CelShading", "vs_4_0", &pVSBlob);
	if (FAILED(phonnHR))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return phonnHR;
	}

	// Create the vertex shader
	phonnHR = g_pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &toonVertexShader);
	if (FAILED(phonnHR))
	{
		pVSBlob->Release();
		return phonnHR;
	}

	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC layout[]
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",	0,	DXGI_FORMAT_R32G32B32_FLOAT, 0 , 20, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT numElements = ARRAYSIZE(layout);


	// Create the input layout
	phonnHR = g_pd3dDevice->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(),
		pVSBlob->GetBufferSize(), &g_pVertexLayout);
	pVSBlob->Release();
	if (FAILED(phonnHR))
		return phonnHR;

	// Set the input layout
	g_pImmediateContext->IASetInputLayout(g_pVertexLayout);


	// Compile the pixel shader
	ID3DBlob*  pPSBlob = NULL;
	//phonnHR = CompileShaderFromFile(L"Shaders.fx", "PS_OutlineShader", "ps_4_0", &pPSBlob);
	//if (FAILED(phonnHR))
	//{
	//	MessageBox(NULL,
	//		L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
	//	return phonnHR;
	//}
	//
	//// Create the pixel shader
	//phonnHR = g_pd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &outlinePixelShader);
	//pPSBlob->Release();
	//if (FAILED(phonnHR))
	//	return phonnHR;

	// Compile the pixel shader
	pPSBlob = NULL;
	phonnHR = CompileShaderFromFile(L"Shaders.fx", "PS_CelShading", "ps_4_0", &pPSBlob);
	if (FAILED(phonnHR))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return phonnHR;
	}

	// Create the pixel shader
	phonnHR = g_pd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &toonPixelShader);
	pPSBlob->Release();
	if (FAILED(phonnHR))
		return phonnHR;







	return S_OK;
}


HRESULT normalBuffer() {
	RECT rc;
	GetClientRect(g_hWnd, &rc);
	UINT width = rc.right - rc.left;
	UINT height = rc.bottom - rc.top;


	//Shadow Mapping RENDER TARGET ***********
	HRESULT hr = S_OK;

	//create shadow map texture desc
	D3D11_TEXTURE2D_DESC texDesc;
	texDesc.Width = width;
	texDesc.Height = height;
	texDesc.MipLevels = 1;
	texDesc.ArraySize = 1;
	texDesc.Format = DXGI_FORMAT_R32_TYPELESS;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Usage = D3D11_USAGE_DEFAULT;
	texDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = 0;



	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory(&descDepth, sizeof(descDepth));
	descDepth.Width = width;
	descDepth.Height = height;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	descDepth.SampleDesc.Count = 1;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;


	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));
	descDSV.Format = descDepth.Format;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;

	//create shader resource view desc
	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	ZeroMemory(&descDSV, sizeof(srvDesc));
	srvDesc.Format = descDepth.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = descDepth.MipLevels;
	srvDesc.Texture2D.MostDetailedMip = 0;


	//create texture and depth/resource views
	hr = g_pd3dDevice->CreateTexture2D(&descDepth, NULL, &normalMap);
	if (FAILED(hr))
		return hr;

	D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
	renderTargetViewDesc.Format = descDepth.Format;
	renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	renderTargetViewDesc.Texture2D.MipSlice = 0;

	hr = g_pd3dDevice->CreateRenderTargetView(normalMap, &renderTargetViewDesc, &normalRenderTargetView);
	if (FAILED(hr))
		return hr;

	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
	shaderResourceViewDesc.Format = descDepth.Format;
	shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
	shaderResourceViewDesc.Texture2D.MipLevels = 1;


	hr = g_pd3dDevice->CreateShaderResourceView(normalMap, &shaderResourceViewDesc, &normalMapSRView);
	if (FAILED(hr))
		return hr;



	//Shadow Mapping RENDER TARGET ***********
	//shadowMap Vertexshader
	ID3DBlob* VS_depthBlob = NULL;
	hr = CompileShaderFromFile(L"Shaders.fx", "VS_PassThru", "vs_4_0", &VS_depthBlob);
	if (FAILED(hr))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return hr;
	}

	// Create the vertex shader
	hr = g_pd3dDevice->CreateVertexShader(VS_depthBlob->GetBufferPointer(), VS_depthBlob->GetBufferSize(), NULL, &normalVertexShader);
	if (FAILED(hr))
	{
		VS_depthBlob->Release();
		return hr;
	}

	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC layout[]
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",	0,	DXGI_FORMAT_R32G32B32_FLOAT, 0 , 20, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT numElements = ARRAYSIZE(layout);


	// Create the input layout
	hr = g_pd3dDevice->CreateInputLayout(layout, numElements, VS_depthBlob->GetBufferPointer(),
		VS_depthBlob->GetBufferSize(), &g_pVertexLayout);
	VS_depthBlob->Release();
	if (FAILED(hr))
		return hr;

	// Set the input layout
	g_pImmediateContext->IASetInputLayout(g_pVertexLayout);

	// Compile the pixel shader
	ID3DBlob* PS_depthBlob = NULL;
	hr = CompileShaderFromFile(L"Shaders.fx", "PS_NormalBuffer", "ps_4_0", &PS_depthBlob);
	if (FAILED(hr))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return hr;
	}

	// Create the pixel shader
	hr = g_pd3dDevice->CreatePixelShader(PS_depthBlob->GetBufferPointer(), PS_depthBlob->GetBufferSize(), NULL, &normalPixelShader);
	PS_depthBlob->Release();
	if (FAILED(hr))
		return hr;



	return S_OK;
}

HRESULT depthBuffer() {

	RECT rc;
	GetClientRect(g_hWnd, &rc);
	UINT width = rc.right - rc.left;
	UINT height = rc.bottom - rc.top;


	//Shadow Mapping RENDER TARGET ***********
	HRESULT shadowrenderTarget = S_OK;

	//create shadow map texture desc
	D3D11_TEXTURE2D_DESC texDesc;
	texDesc.Width = width;
	texDesc.Height = height;
	texDesc.MipLevels = 1;
	texDesc.ArraySize = 1;
	texDesc.Format = DXGI_FORMAT_R32_TYPELESS;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Usage = D3D11_USAGE_DEFAULT;
	texDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = 0;



	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory(&descDepth, sizeof(descDepth));
	descDepth.Width = width;
	descDepth.Height = height;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	descDepth.SampleDesc.Count = 1;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;

	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));
	descDSV.Format = descDepth.Format;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;

	//create shader resource view desc
	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	ZeroMemory(&descDSV, sizeof(srvDesc));
	srvDesc.Format = descDepth.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = descDepth.MipLevels;
	srvDesc.Texture2D.MostDetailedMip = 0;


	//create texture and depth/resource views
	shadowrenderTarget = g_pd3dDevice->CreateTexture2D(&descDepth, NULL, &depthMap);
	if (FAILED(shadowrenderTarget))
		return shadowrenderTarget;

	D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
	renderTargetViewDesc.Format = descDepth.Format;
	renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	renderTargetViewDesc.Texture2D.MipSlice = 0;

	shadowrenderTarget = g_pd3dDevice->CreateRenderTargetView(depthMap, &renderTargetViewDesc, &depthRenderTargetView);
	if (FAILED(shadowrenderTarget))
		return shadowrenderTarget;

	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
	shaderResourceViewDesc.Format = descDepth.Format;
	shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
	shaderResourceViewDesc.Texture2D.MipLevels = 1;


	shadowrenderTarget = g_pd3dDevice->CreateShaderResourceView(depthMap, &shaderResourceViewDesc, &depthMapSRView);
	if (FAILED(shadowrenderTarget))
		return shadowrenderTarget;


	//Shadow Mapping RENDER TARGET ***********
	//shadowMap Vertexshader
	ID3DBlob* VS_depthBlob = NULL;
	shadowrenderTarget = CompileShaderFromFile(L"Shaders.fx", "VS_PassThru", "vs_4_0", &VS_depthBlob);
	if (FAILED(shadowrenderTarget))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return shadowrenderTarget;
	}

	// Create the vertex shader
	shadowrenderTarget = g_pd3dDevice->CreateVertexShader(VS_depthBlob->GetBufferPointer(), VS_depthBlob->GetBufferSize(), NULL, &depthVertexShader);
	if (FAILED(shadowrenderTarget))
	{
		VS_depthBlob->Release();
		return shadowrenderTarget;
	}

	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC layout[]
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",	0,	DXGI_FORMAT_R32G32B32_FLOAT, 0 , 20, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT numElements = ARRAYSIZE(layout);


	// Create the input layout
	shadowrenderTarget = g_pd3dDevice->CreateInputLayout(layout, numElements, VS_depthBlob->GetBufferPointer(),
		VS_depthBlob->GetBufferSize(), &g_pVertexLayout);
	VS_depthBlob->Release();
	if (FAILED(shadowrenderTarget))
		return shadowrenderTarget;

	// Set the input layout
	g_pImmediateContext->IASetInputLayout(g_pVertexLayout);

	// Compile the pixel shader
	ID3DBlob* PS_depthBlob = NULL;
	shadowrenderTarget = CompileShaderFromFile(L"Shaders.fx", "PS_Depth", "ps_4_0", &PS_depthBlob);
	if (FAILED(shadowrenderTarget))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return shadowrenderTarget;
	}

	// Create the pixel shader
	shadowrenderTarget = g_pd3dDevice->CreatePixelShader(PS_depthBlob->GetBufferPointer(), PS_depthBlob->GetBufferSize(), NULL, &depthPixelShader);
	PS_depthBlob->Release();
	if (FAILED(shadowrenderTarget))
		return shadowrenderTarget;



	return S_OK;
}


HRESULT ssaoInit() {

	RECT rc;
	GetClientRect(g_hWnd, &rc);
	UINT width = rc.right - rc.left;
	UINT height = rc.bottom - rc.top;

	HRESULT phonnHR = S_OK;


	// Create a render target view
	ID3D11Texture2D* pBackBuffer = NULL;
	phonnHR = g_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
	if (FAILED(phonnHR))
		return phonnHR;

	phonnHR = g_pd3dDevice->CreateRenderTargetView(pBackBuffer, NULL, &ssaoRenderTargetView);
	pBackBuffer->Release();
	if (FAILED(phonnHR))
		return phonnHR;


	//Render Specular
	g_pImmediateContext->RSSetViewports(1, &phonnViewPort);


	// Compile the vertex shader
	ID3DBlob* pVSBlob = NULL;
	phonnHR = CompileShaderFromFile(L"Shaders.fx", "VS_FULLSCREENQUAD", "vs_4_0", &pVSBlob);
	if (FAILED(phonnHR))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return phonnHR;
	}

	// Create the vertex shader
	phonnHR = g_pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &ssaoVertexShader);
	if (FAILED(phonnHR))
	{
		pVSBlob->Release();
		return phonnHR;
	}

	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC layout[]
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",	0,	DXGI_FORMAT_R32G32B32_FLOAT, 0 , 20, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT numElements = ARRAYSIZE(layout);


	// Create the input layout
	phonnHR = g_pd3dDevice->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(),
		pVSBlob->GetBufferSize(), &g_pVertexLayout);
	pVSBlob->Release();
	if (FAILED(phonnHR))
		return phonnHR;

	// Set the input layout
	g_pImmediateContext->IASetInputLayout(g_pVertexLayout);

	// Compile the pixel shader
	ID3DBlob* pPSBlob = NULL;
	phonnHR = CompileShaderFromFile(L"Shaders.fx", "PS_SSAO", "ps_4_0", &pPSBlob);
	if (FAILED(phonnHR))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return phonnHR;
	}

	// Create the pixel shader
	phonnHR = g_pd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &ssaoPixelShader);
	pPSBlob->Release();
	if (FAILED(phonnHR))
		return phonnHR;


	return S_OK;
}

//--------------------------------------------------------------------------------------
// Clean up the objects we've created
//--------------------------------------------------------------------------------------
void CleanupDevice()
{
    if( g_pImmediateContext ) g_pImmediateContext->ClearState();




	//Shadow Mapping 
	if (pShadowRenderTargetView) pShadowRenderTargetView->Release();
	if (pShadowMap) pShadowMap->Release();
	if (pShadowMapDepthView) pShadowMapDepthView->Release();
	if (pShadowMapSRView) pShadowMapSRView->Release();
	if (pSamplerLinear) pSamplerLinear->Release();

    if( g_pSamplerLinear ) g_pSamplerLinear->Release();
    if( g_pTextureRV ) g_pTextureRV->Release();
    if( g_pCBNeverChanges ) g_pCBNeverChanges->Release();
    if( g_pCBChangeOnResize ) g_pCBChangeOnResize->Release();
    if( g_pCBChangesEveryFrame ) g_pCBChangesEveryFrame->Release();
    if( g_pVertexBuffer ) g_pVertexBuffer->Release();
    if( g_pIndexBuffer ) g_pIndexBuffer->Release();
    if( g_pVertexLayout ) g_pVertexLayout->Release();
    if( g_pVertexShader ) g_pVertexShader->Release();
    if( g_pPixelShader ) g_pPixelShader->Release();
    if( g_pDepthStencil ) g_pDepthStencil->Release();
    if( g_pDepthStencilView ) g_pDepthStencilView->Release();
    if( g_pRenderTargetView ) g_pRenderTargetView->Release();
    if( g_pSwapChain ) g_pSwapChain->Release();
    if( g_pImmediateContext ) g_pImmediateContext->Release();

	if (phongDepthStencil) phongDepthStencil->Release();
	if (phongDepthStencilView) phongDepthStencilView->Release();
	if (phongVertexShader) phongVertexShader->Release();
	if (phongPixelShader) phongPixelShader->Release();
	if (phongRenderTarget) phongRenderTarget->Release();
	if (phonnMap) phonnMap->Release();
	if (phonnMapRSView) phonnMapRSView->Release();

	if (outlinePixelShader) outlinePixelShader->Release();
	if (outlineVertexShader) outlineVertexShader->Release();
	if (toonVertexShader) toonVertexShader->Release();
	if (toonPixelShader) toonPixelShader->Release();
	if (toonMap) toonMap->Release();
	if (toonMapDepthView) toonMapDepthView->Release();
	if (toonRenderTargetView) toonRenderTargetView->Release();
	if (toonMapSRView) toonMapSRView->Release();
	if (qMap) qMap->Release();


	if (normalVertexShader) normalVertexShader->Release();
	if (normalPixelShader) normalPixelShader->Release();
	if (normalMap) normalMap->Release();
	if (normalMapDepthView) normalMapDepthView->Release();
	if (normalRenderTargetView) normalRenderTargetView->Release();
	if (normalMapSRView) normalMapSRView->Release();

	if (depthVertexShader) depthVertexShader->Release();
	if (depthPixelShader) depthPixelShader->Release();
	if (depthMap) depthMap->Release();
	if (depthMapDepthView) depthMapDepthView->Release();
	if (depthRenderTargetView) depthRenderTargetView->Release();
	if (depthMapSRView) depthMapSRView->Release();

	if (ssaoVertexShader) ssaoVertexShader->Release();
	if (ssaoPixelShader) ssaoPixelShader->Release();
	if (ssaoMap) ssaoMap->Release();
	if (ssaoMapDepthView) ssaoMapDepthView->Release();
	if (ssaoRenderTargetView) ssaoRenderTargetView->Release();
	if (ssaoMapSRView) ssaoMapSRView->Release();


	for (int i = 0; i < meshes.size(); i++) {
		meshes[i].Release();
	}
	
    if( g_pd3dDevice ) g_pd3dDevice->Release();


}



//--------------------------------------------------------------------------------------
// Called every time the application receives a message
//--------------------------------------------------------------------------------------
LRESULT CALLBACK WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
    PAINTSTRUCT ps;
    HDC hdc;

    switch( message )
    {
        case WM_PAINT:
            hdc = BeginPaint( hWnd, &ps );
            EndPaint( hWnd, &ps );
            break;

        case WM_DESTROY:
            PostQuitMessage( 0 );
            break;

        default:
            return DefWindowProc( hWnd, message, wParam, lParam );
    }

    return 0;
}


//--------------------------------------------------------------------------------------
// Render a frame
//--------------------------------------------------------------------------------------
void Render()
{
    // Update our time
    static float t = 0.0f;
    if( g_driverType == D3D_DRIVER_TYPE_REFERENCE )
    {
        t += ( float )XM_PI * 0.0125f;
    }
    else
    {
        static DWORD dwTimeStart = 0;
        DWORD dwTimeCur = GetTickCount();
        if( dwTimeStart == 0 )
            dwTimeStart = dwTimeCur;
        t = ( dwTimeCur - dwTimeStart ) / 1000.0f;
    }

    // Rotate cube around the origin
    g_World = XMMatrixRotationY( t );

    // Modify the color
    g_vMeshColor.x = ( sinf( t * 1.0f ) + 1.0f ) * 0.5f;
    g_vMeshColor.y = ( cosf( t * 3.0f ) + 1.0f ) * 0.5f;
    g_vMeshColor.z = ( sinf( t * 5.0f ) + 1.0f ) * 0.5f;

    //
    // Clear the back buffer
    //

	//Render to texture
	//g_pImmediateContext->OMSetRenderTargets(1, &pShadowRenderTargetView, pShadowMapDepthView);

    float ClearColor[4] = { 0.3f, 0.3f, 0.3f, 1.0f }; // red, green, blue, alpha
	g_pImmediateContext->ClearRenderTargetView(normalRenderTargetView, ClearColor);
	g_pImmediateContext->ClearRenderTargetView(pShadowRenderTargetView, ClearColor);
    g_pImmediateContext->ClearRenderTargetView( g_pRenderTargetView, ClearColor );
	g_pImmediateContext->ClearRenderTargetView(phongRenderTarget, ClearColor);
	g_pImmediateContext->ClearRenderTargetView(toonRenderTargetView, ClearColor);
	g_pImmediateContext->ClearRenderTargetView(depthRenderTargetView, ClearColor);
	g_pImmediateContext->ClearRenderTargetView(ssaoRenderTargetView, ClearColor);




    //
    // Clear the depth buffer to 1.0 (max depth)
    //
    g_pImmediateContext->ClearDepthStencilView( g_pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0 );


	// Set vertex buffer
	UINT stride = sizeof(VertexType);
	//UINT stride = sizeof(SimpleVertex);
	UINT offset = 0;
	// Set primitive topology
	g_pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

    //
    // Update variables that change once per frame
    //
	CBChangesEveryFrame cb;
	cb.Camera = XMVectorSet(0.0f, 10.0f, -20.0f, 0.0f);
    cb.mWorld = XMMatrixTranspose( g_World );
    cb.vMeshColor = g_vMeshColor;
    g_pImmediateContext->UpdateSubresource( g_pCBChangesEveryFrame, 0, NULL, &cb, 0, 0 );


	//Transfer in shadow texture
	g_pImmediateContext->RSSetViewports(1, &fullScreenViewport);


	//Set rendertarget NormalMap
	g_pImmediateContext->OMSetRenderTargets(1, &depthRenderTargetView, depthMapDepthView);

	int currMeshID = 0;
	g_pImmediateContext->VSSetShader(depthVertexShader, NULL, 0);
	g_pImmediateContext->VSSetConstantBuffers(0, 1, &g_pCBNeverChanges);
	g_pImmediateContext->VSSetConstantBuffers(1, 1, &g_pCBChangeOnResize);
	g_pImmediateContext->VSSetConstantBuffers(2, 1, &g_pCBChangesEveryFrame);
	g_pImmediateContext->PSSetShader(depthPixelShader, NULL, 0);
	g_pImmediateContext->PSSetConstantBuffers(2, 1, &g_pCBChangesEveryFrame);
	g_pImmediateContext->PSSetShaderResources(0, 1, &g_pTextureRV);
	g_pImmediateContext->PSSetSamplers(0, 1, &g_pSamplerLinear);
	for (unsigned int i = 0; i < meshes.size(); i++)
	{

		g_pImmediateContext->IASetVertexBuffers(0, 1, &meshes[i].pVertexBuffer, &meshes[i].stride, &meshes[i].offset);
		g_pImmediateContext->IASetIndexBuffer(meshes[i].pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);



		g_pImmediateContext->DrawIndexed(meshes[i].obj.m_indexCount, 0, 0);
	}

	g_pImmediateContext->ClearRenderTargetView(normalRenderTargetView, ClearColor);

	//Set rendertarget NormalMap
	g_pImmediateContext->OMSetRenderTargets(1, &normalRenderTargetView, normalMapDepthView);

	currMeshID = 0;
	g_pImmediateContext->VSSetShader(normalVertexShader, NULL, 0);
	g_pImmediateContext->VSSetConstantBuffers(0, 1, &g_pCBNeverChanges);
	g_pImmediateContext->VSSetConstantBuffers(1, 1, &g_pCBChangeOnResize);
	g_pImmediateContext->VSSetConstantBuffers(2, 1, &g_pCBChangesEveryFrame);
	g_pImmediateContext->PSSetShader(normalPixelShader, NULL, 0);
	g_pImmediateContext->PSSetConstantBuffers(2, 1, &g_pCBChangesEveryFrame);
	g_pImmediateContext->PSSetShaderResources(0, 1, &g_pTextureRV);
	g_pImmediateContext->PSSetSamplers(0, 1, &g_pSamplerLinear);
	for (unsigned int i = 0; i < meshes.size(); i++)
	{

		g_pImmediateContext->IASetVertexBuffers(0, 1, &meshes[i].pVertexBuffer, &meshes[i].stride, &meshes[i].offset);
		g_pImmediateContext->IASetIndexBuffer(meshes[i].pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);



		g_pImmediateContext->DrawIndexed(meshes[i].obj.m_indexCount, 0, 0);
	}

	g_pImmediateContext->ClearRenderTargetView(pShadowRenderTargetView, ClearColor);

	//Set rendertarget to lightDepthMap
	g_pImmediateContext->OMSetRenderTargets(1, &pShadowRenderTargetView, pShadowMapDepthView);

	currMeshID = 0;
	g_pImmediateContext->VSSetShader(pVertexShader, NULL, 0);
	g_pImmediateContext->VSSetConstantBuffers(0, 1, &g_pCBNeverChanges);
	g_pImmediateContext->VSSetConstantBuffers(1, 1, &g_pCBChangeOnResize);
	g_pImmediateContext->VSSetConstantBuffers(2, 1, &g_pCBChangesEveryFrame);
	g_pImmediateContext->PSSetShader(pPixelShader, NULL, 0);
	g_pImmediateContext->PSSetConstantBuffers(2, 1, &g_pCBChangesEveryFrame);
	g_pImmediateContext->PSSetShaderResources(0, 1, &g_pTextureRV);
	g_pImmediateContext->PSSetSamplers(0, 1, &g_pSamplerLinear);
	for (unsigned int i = 0; i < meshes.size(); i++)
	{

		g_pImmediateContext->IASetVertexBuffers(0, 1, &meshes[i].pVertexBuffer, &meshes[i].stride, &meshes[i].offset);
		g_pImmediateContext->IASetIndexBuffer(meshes[i].pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);



		g_pImmediateContext->DrawIndexed(meshes[i].obj.m_indexCount, 0, 0);
	}





	//Transfer in shadow texture
	g_pImmediateContext->RSSetViewports(1, &shadowMapViewport);
	//Set rendertarget to scene
	g_pImmediateContext->OMSetRenderTargets(1, &g_pRenderTargetView, g_pDepthStencilView);

	g_pImmediateContext->VSSetShader(pVertexShader, NULL, 0);
	g_pImmediateContext->VSSetConstantBuffers(0, 1, &g_pCBNeverChanges);
	g_pImmediateContext->VSSetConstantBuffers(1, 1, &g_pCBChangeOnResize);
	g_pImmediateContext->VSSetConstantBuffers(2, 1, &g_pCBChangesEveryFrame);
	g_pImmediateContext->PSSetShader(pPixelShader, NULL, 0);
	g_pImmediateContext->PSSetConstantBuffers(2, 1, &g_pCBChangesEveryFrame);
	g_pImmediateContext->PSSetShaderResources(0, 1, &g_pTextureRV);
	g_pImmediateContext->PSSetSamplers(0, 1, &g_pSamplerLinear);
	currMeshID = 0;
	for (unsigned int i = 0; i < meshes.size(); i++)
	{

		g_pImmediateContext->IASetVertexBuffers(0, 1, &meshes[i].pVertexBuffer, &meshes[i].stride, &meshes[i].offset);
		g_pImmediateContext->IASetIndexBuffer(meshes[i].pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);


		g_pImmediateContext->DrawIndexed(meshes[i].obj.m_indexCount, 0, 0);
	}

    //
    // Render the object
    //
	g_pImmediateContext->RSSetViewports(1, &vp);

	//Set rendertarget to scene
	
	g_pImmediateContext->VSSetShader(g_pVertexShader, NULL, 0);
	g_pImmediateContext->VSSetConstantBuffers(0, 1, &g_pCBNeverChanges);
	g_pImmediateContext->VSSetConstantBuffers(1, 1, &g_pCBChangeOnResize);
	g_pImmediateContext->VSSetConstantBuffers(2, 1, &g_pCBChangesEveryFrame);
	g_pImmediateContext->PSSetShader(g_pPixelShader, NULL, 0);
	g_pImmediateContext->PSSetConstantBuffers(2, 1, &g_pCBChangesEveryFrame);
	g_pImmediateContext->PSSetShaderResources(0, 1, &g_pTextureRV);
	g_pImmediateContext->PSSetShaderResources(1, 1, &pShadowMapSRView);
	g_pImmediateContext->PSSetSamplers(0, 1, &g_pSamplerLinear);
	g_pImmediateContext->PSSetSamplers(1, 1, &pSamplerLinear);
	currMeshID = 0;
	for (unsigned int i = 0; i < meshes.size(); i++)
	{

		g_pImmediateContext->IASetVertexBuffers(0, 1, &meshes[i].pVertexBuffer, &meshes[i].stride, &meshes[i].offset);
		g_pImmediateContext->IASetIndexBuffer(meshes[i].pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);


		g_pImmediateContext->DrawIndexed(meshes[i].obj.m_indexCount, 0, 0);
	}



	//Render Specular
	g_pImmediateContext->RSSetViewports(1, &phonnViewPort);

	//Set rendertarget to scene
	//g_pImmediateContext->OMSetRenderTargets(1, &phongRenderTarget, g_pDepthStencilView);
	//
	//
	//g_pImmediateContext->VSSetShader(phongVertexShader, NULL, 0);
	//g_pImmediateContext->VSSetConstantBuffers(0, 1, &g_pCBNeverChanges);
	//g_pImmediateContext->VSSetConstantBuffers(1, 1, &g_pCBChangeOnResize);
	//g_pImmediateContext->VSSetConstantBuffers(2, 1, &g_pCBChangesEveryFrame);
	//g_pImmediateContext->PSSetShader(phongPixelShader, NULL, 0);
	//g_pImmediateContext->PSSetConstantBuffers(2, 1, &g_pCBChangesEveryFrame);
	//g_pImmediateContext->PSSetShaderResources(0, 1, &g_pTextureRV);
	//g_pImmediateContext->PSSetSamplers(0, 1, &g_pSamplerLinear);
	//g_pImmediateContext->PSSetSamplers(1, 1, &pSamplerLinear);
	//currMeshID = 0;
	//for (unsigned int i = 0; i < meshes.size(); i++)
	//{
	//
	//	g_pImmediateContext->IASetVertexBuffers(0, 1, &meshes[i].pVertexBuffer, &meshes[i].stride, &meshes[i].offset);
	//	g_pImmediateContext->IASetIndexBuffer(meshes[i].pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	//
	//
	//	g_pImmediateContext->DrawIndexed(meshes[i].obj.m_indexCount, 0, 0);
	//}
	g_pImmediateContext->OMSetRenderTargets(1, &ssaoRenderTargetView, g_pDepthStencilView);
	g_pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);


	DisableDepthTest();


	g_pImmediateContext->VSSetShader(ssaoVertexShader, NULL, 0);
	g_pImmediateContext->VSSetConstantBuffers(0, 1, &g_pCBNeverChanges);
	g_pImmediateContext->VSSetConstantBuffers(1, 1, &g_pCBChangeOnResize);
	g_pImmediateContext->VSSetConstantBuffers(2, 1, &g_pCBChangesEveryFrame);
	g_pImmediateContext->PSSetShader(ssaoPixelShader, NULL, 0);
	g_pImmediateContext->PSSetConstantBuffers(2, 1, &g_pCBChangesEveryFrame);
	g_pImmediateContext->PSSetShaderResources(0, 1, &g_pTextureRV);
	g_pImmediateContext->PSSetShaderResources(2, 1, &qMap);
	g_pImmediateContext->PSSetShaderResources(3, 1, &normalMapSRView);
	g_pImmediateContext->PSSetShaderResources(4, 1, &depthMapSRView);
	g_pImmediateContext->PSSetSamplers(0, 1, &g_pSamplerLinear);
	g_pImmediateContext->PSSetSamplers(1, 1, &pSamplerLinear);
	//currMeshID = 0;
	//for (unsigned int i = 0; i < meshes.size(); i++)
	//{
	//
	//	g_pImmediateContext->IASetVertexBuffers(0, 1, &meshes[i].pVertexBuffer, &meshes[i].stride, &meshes[i].offset);
	//	g_pImmediateContext->IASetIndexBuffer(meshes[i].pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	//
	//
	//	g_pImmediateContext->DrawIndexed(meshes[i].obj.m_indexCount, 0, 0);
	//}
	//g_pImmediateContext->IASetVertexBuffers(0, 0, NULL, 0, 0);
	//g_pImmediateContext->IASetVertexBuffers(1, 0, NULL, 0, 0);
	//g_pImmediateContext->IASetIndexBuffer(NULL, DXGI_FORMAT_R32_UINT, 0);
	g_pImmediateContext->DrawIndexed(4, 0, 0);


	g_pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	EnableDepthTest();
	//Render Specular
	g_pImmediateContext->RSSetViewports(1, &toonViewPort);

	//Set rendertarget to scene
	g_pImmediateContext->OMSetRenderTargets(1, &toonRenderTargetView, g_pDepthStencilView);

	g_pImmediateContext->VSSetShader(outlineVertexShader, NULL, 0);
	g_pImmediateContext->VSSetConstantBuffers(0, 1, &g_pCBNeverChanges);
	g_pImmediateContext->VSSetConstantBuffers(1, 1, &g_pCBChangeOnResize);
	g_pImmediateContext->VSSetConstantBuffers(2, 1, &g_pCBChangesEveryFrame);
	g_pImmediateContext->PSSetShader(outlinePixelShader, NULL, 0);
	g_pImmediateContext->PSSetConstantBuffers(2, 1, &g_pCBChangesEveryFrame);
	g_pImmediateContext->PSSetShaderResources(0, 1, &g_pTextureRV);
	currMeshID = 0;
	for (unsigned int i = 0; i < meshes.size(); i++)
	{
	
		g_pImmediateContext->IASetVertexBuffers(0, 1, &meshes[i].pVertexBuffer, &meshes[i].stride, &meshes[i].offset);
		g_pImmediateContext->IASetIndexBuffer(meshes[i].pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	
	
		g_pImmediateContext->DrawIndexed(meshes[i].obj.m_indexCount, 0, 0);
	
	}

	g_pImmediateContext->VSSetShader(toonVertexShader, NULL, 0);
	g_pImmediateContext->VSSetConstantBuffers(0, 1, &g_pCBNeverChanges);
	g_pImmediateContext->VSSetConstantBuffers(1, 1, &g_pCBChangeOnResize);
	g_pImmediateContext->VSSetConstantBuffers(2, 1, &g_pCBChangesEveryFrame);
	g_pImmediateContext->PSSetShader(toonPixelShader, NULL, 0);
	g_pImmediateContext->PSSetConstantBuffers(2, 1, &g_pCBChangesEveryFrame);
	g_pImmediateContext->PSSetShaderResources(0, 1, &g_pTextureRV);
	g_pImmediateContext->PSSetShaderResources(1, 1, &pShadowMapSRView);
	g_pImmediateContext->PSSetShaderResources(2, 1, &qMap);
	g_pImmediateContext->PSSetSamplers(0, 1, &g_pSamplerLinear);
	g_pImmediateContext->PSSetSamplers(1, 1, &pSamplerLinear);
	currMeshID = 0;
	for (unsigned int i = 0; i < meshes.size(); i++)
	{

		g_pImmediateContext->IASetVertexBuffers(0, 1, &meshes[i].pVertexBuffer, &meshes[i].stride, &meshes[i].offset);
		g_pImmediateContext->IASetIndexBuffer(meshes[i].pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);


		g_pImmediateContext->DrawIndexed(meshes[i].obj.m_indexCount, 0, 0);
	}
	

    //
    // Present our back buffer to our front buffer
    //
    g_pSwapChain->Present( 0, 0 );
}

void DisableDepthTest() {
	D3D11_DEPTH_STENCIL_DESC dsDesc;

	// Depth test parameters
	dsDesc.DepthEnable = false;
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dsDesc.DepthFunc = D3D11_COMPARISON_LESS;

	// Stencil test parameters
	dsDesc.StencilEnable = false;
	dsDesc.StencilReadMask = 0xFF;
	dsDesc.StencilWriteMask = 0xFF;

	// Stencil operations if pixel is front-facing
	dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Stencil operations if pixel is back-facing
	dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Create depth stencil state
	ID3D11DepthStencilState * pDSState;
	g_pd3dDevice->CreateDepthStencilState(&dsDesc, &pDSState);
}
void EnableDepthTest() {
	D3D11_DEPTH_STENCIL_DESC dsDesc;

	// Depth test parameters
	dsDesc.DepthEnable = true;
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dsDesc.DepthFunc = D3D11_COMPARISON_LESS;

	// Stencil test parameters
	dsDesc.StencilEnable = true;
	dsDesc.StencilReadMask = 0xFF;
	dsDesc.StencilWriteMask = 0xFF;

	// Stencil operations if pixel is front-facing
	dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Stencil operations if pixel is back-facing
	dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Create depth stencil state
	ID3D11DepthStencilState * pDSState;
	g_pd3dDevice->CreateDepthStencilState(&dsDesc, &pDSState);
}

//XMFLOAT3* generateSamples(XMFLOAT3 kernel[]) {
//	float LO = -1.0f;
//	float HI = 1.0f;
//	for (int i = 0; i < 64; ++i) {
//		kernel[i] = XMFLOAT3(
//			LO + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (HI - LO))),
//			LO + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (HI - LO))),
//			static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (HI))));
//	}
//	return kernel;
//}