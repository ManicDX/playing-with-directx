#pragma once

#include <windows.h>
#include <d3d11.h>
#include <d3dx11.h>
#include <d3dcompiler.h>
#include <xnamath.h>
//#include "resource.h"
#include <d3dx10.h>

#include "OBJModel.h"

//// include the Direct3D Library file
#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "d3dx11.lib")
#pragma comment (lib, "d3dx10.lib")



class Mesh
{
public:
	//size values
	unsigned int size, numVertices, numIndices;
	UINT stride, offset;

	OBJModel obj;

	Mesh();
	~Mesh();

	//vertex and index buffers
	ID3D11Buffer* pVertexBuffer;
	ID3D11Buffer* pIndexBuffer;

	///create and release methods
	bool CreateMesh(ID3D11Device* g_pd3dDevice, const char* filePath);
	void Release();

	
};

